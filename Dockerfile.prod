
# Stage 1
FROM node:20-alpine as dependencies

WORKDIR /app

COPY package.json package-lock.json ./
COPY .env/.env.production .env/.env.production

RUN npm i

#Stage 2 - Build
FROM node:20-alpine as builder

WORKDIR /app

COPY --from=dependencies /app/node_modules ./node_modules
COPY --from=dependencies /app/.env/.env.production .env/.env.production
COPY . .

RUN npm run build:prod

# Debugging step to list the contents of the standalone directory
RUN ls -l /app/.next/standalone

# Stage 3 Runner
FROM node:20-alpine as runner

WORKDIR /app

COPY --from=builder /app/.next/standalone /app/standalone
COPY --from=builder /app/public /app/standalone/public
COPY --from=builder /app/.next/static /app/standalone/.next/static
COPY --from=builder /app/.env/.env.production /app/standalone/.env/.env.production

EXPOSE 3000

CMD HOSTNAME="0.0.0.0" node ./standalone/server.js