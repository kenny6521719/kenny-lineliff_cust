const i18nConfig = {
  locales: ['th', 'en'],
  defaultLocale: 'th',
  localeCookie: 'NEXT_LOCALE',
  localeDetection: false,
  prefixDefault: true
};

module.exports = i18nConfig;