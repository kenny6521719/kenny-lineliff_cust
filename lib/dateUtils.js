import { format, parse } from "date-fns"

// Function สำหรับการตรวจสอบและเพิ่มจำนวนชั่วโมง
export function addHours(timeString, hoursToAdd) {
    if (hoursToAdd == null) {
        hoursToAdd = 0;
    }
    
    // ตรวจสอบรูปแบบของ timeString
    const timePattern = /^\d{2}:\d{2}$/;
    if (!timeString.match(timePattern)) {
        return "";
    }

    const [hours, minutes] = timeString.split(':').map(Number);

    // ตรวจสอบว่าชั่วโมงและนาทีเป็นตัวเลขที่ถูกต้อง
    if (hours < 0 || hours > 23 || minutes < 0 || minutes > 59) {
        return "";
    }

    let date = new Date();
    date.setHours(hours, minutes);
    date.setHours(date.getHours() + hoursToAdd);
    let endHours = date.getHours().toString().padStart(2, '0');
    let endMinutes = date.getMinutes().toString().padStart(2, '0');
    return `${endHours}:${endMinutes}`;
}

export const formatTime = (timeString) => {
    const date = parse(timeString, "HH:mm:ss", new Date())
    return format(date, "HH:mm")
}