export function formatNumberWithThousandSeparator(number, digit) {
    try {
        return new Intl.NumberFormat('en-US', { minimumFractionDigits: digit, maximumFractionDigits: digit }).format(number);
    } catch (error) {
        return '0.00';
    }
}

