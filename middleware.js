import { NextResponse } from "next/server";
import i18nConfig from "./i18n-config"
import { i18nRouter } from 'next-i18n-router';

function getLocale(request) {
  const localeCookie = request.cookies.get('NEXT_LOCALE')?.value;
  
  if (localeCookie && i18nConfig.locales.includes(localeCookie)) {
    return localeCookie;
  }
  
  return i18nConfig.defaultLocale;
}

export function middleware(request) {
  const { pathname, searchParams } = request.nextUrl;

  // // // ไม่ทำ redirect สำหรับ callback URL
  // if (pathname.startsWith('/auth/callback')) {
  //   return NextResponse.next();
  // }

  // ตรวจสอบว่า path เป็น '/' หรือ '/th' หรือไม่
  if (pathname === '/') {
    const locale = getLocale(request);
    // สำหรับ root path หรือ '/th' ที่ locale ไม่ใช่ 'th'
    return NextResponse.redirect(new URL(`/${locale}`, request.url));
  }

  // สำหรับ path อื่นๆ
  const pathnameIsMissingLocale = i18nConfig.locales.every(
    (locale) => !pathname.startsWith(`/${locale}/`) && pathname !== `/${locale}`
  );

  if (pathnameIsMissingLocale) {
    const locale = getLocale(request);
    return NextResponse.redirect(new URL(`/${locale}${pathname}`, request.url));
  }

  return i18nRouter(request, i18nConfig);
}

export const config = {
  matcher: '/((?!api|static|.*\\..*|_next).*)'
};