import dotenv from "dotenv";
import path from 'path';

const envPath = () => {
    return path.resolve(process.cwd(), `.env/.env.${process.env.NEXT_PUBLIC_APP_ENV}`) 
}
console.log('env path', envPath());
dotenv.config({ path: envPath() });

/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    output: "standalone",
};

export default nextConfig;
