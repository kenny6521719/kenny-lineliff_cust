import { create } from 'zustand'
import { persist, createJSONStorage } from 'zustand/middleware'

export const userStore = create(
    persist(
      (set, get) => ({
        userSession: null,
        setUserSession: (newUser) => set({ userSession: newUser }),
      }),
      {
        name: 'auth-storage', // name of the item in the storage (must be unique)
        // storage: createJSONStorage(() => localStorage), // (optional) by default, 'localStorage' is used
      },
    ),
  )