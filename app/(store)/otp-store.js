import { create } from 'zustand'

export const useOtpStore = create((set, get) => ({
  countdown: 0,
  isResendEnabled: true,
  timer: null,
  setCountdown: (count) => set({ countdown: count }),
  setIsResendEnabled: (enabled) => set({ isResendEnabled: enabled }),
  startCountdown: () => {
    // Clear any existing timer
    if (get().timer) {
      clearInterval(get().timer);
    }

    set({ countdown: 30, isResendEnabled: false });
    const timer = setInterval(() => {
      set((state) => {
        if (state.countdown > 0) {
          return { countdown: state.countdown - 1 };
        } else {
          clearInterval(timer);
          return { isResendEnabled: true, timer: null };
        }
      });
    }, 1000);
    set({ timer });
  },
  resetCountdown: () => {
    // Clear the existing timer if it's running
    if (get().timer) {
      clearInterval(get().timer);
    }
    // Reset the state
    set({ countdown: 30, isResendEnabled: false, timer: null });
  }
}))