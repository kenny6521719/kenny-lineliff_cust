import { create } from 'zustand'
import { persist, createJSONStorage } from 'zustand/middleware'

export const cartStore = create(
    persist(
        (set, get) => ({
            cart: null,
            setCart: (item) => {
                set(state => ({
                    cart: { ...state.cart, ...item }
                }))
            },
            removeCart: () => set({ cart: null }),
        }),
        { name: 'cart-storage' }
    )
)