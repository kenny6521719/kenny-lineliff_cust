import { create } from 'zustand'
import { persist, createJSONStorage } from 'zustand/middleware'

export const addressHandlerStore = create(
    persist(
        (set, get) => ({
            address: null,
            setAddress: (item) => {
                set(state => ({
                    address: { ...state.address, ...item }
                }))
            },
            removeAddress: () => set({ address: null }),
        }),
        { name: 'address-info-storage' }
    )
)