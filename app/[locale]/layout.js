import { Inter } from "next/font/google";
import { Kanit } from 'next/font/google'
import "../globals.css";
import { LiffProvider } from '../providers/LiffProvider'
import i18nConfig from "../../i18n-config"
import { dir } from 'i18next';
import NavigationBar from "@/components/navbar/navigation-bar"
import TranslationsProvider from '@/app/providers/TranslationProvider'
import initTranslations from '../i18n'
import { cn } from "@/lib/utils"
import { icons } from "lucide-react"
import ReactQueryClientProvider from '@/app/providers/ReactQueryClientProvider'
import { Toaster } from "@/components/ui/toaster"

const inter = Inter({ subsets: ["latin"] });
const kanit = Kanit({
    weight: ['100', '200', '300', '400', '500', '600', '700', '800', '900'],
    subsets: ['latin', 'thai'],
    variable: '--font-kanit',
})

export const metadata = {
    title: "KENNY Services",
    description: "",
};

export const viewport = {
    width: 'device-width',
    initialScale: 1,
    maximumScale: 1,
    userScalable: false,
    // Also supported by less commonly used
    // interactiveWidget: 'resizes-visual',
}

export async function generateStaticParams() {
    return i18nConfig.locales.map((locale) => ({ locale }));
}


export default async function RootLayout({ children, params: { locale } }) {
    const i18nNamespace = ['navbar'];
    const { resources } = await initTranslations(locale, i18nNamespace);

    return (
        <LiffProvider liffId={process.env.NEXT_PUBLIC_LIFF_ID || ''}>
            <ReactQueryClientProvider>
                <html lang={locale} className={`${kanit.variable} font-sans`}>
                    {/* <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/> */}
                    <body >
                        <link rel="icon" href="/logo/icon.png" sizes="any" />
                        <div className="flex flex-col max-w-screen">
                            <TranslationsProvider
                                namespaces={i18nNamespace}
                                locale={locale}
                                resources={resources}>
                                <NavigationBar />
                            </TranslationsProvider>
                            <main className="mt-20">{children}</main>
                        </div>
                        <Toaster />
                    </body>
                </html>
            </ReactQueryClientProvider>
        </LiffProvider>
    );
}
