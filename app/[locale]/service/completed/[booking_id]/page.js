"use client"

import React, { useEffect, useRef } from "react"
import Image from 'next/image'
import PromptpayLogo from "@/public/logo/prompt-pay-logo.jpg"
import { Separator } from "@/components/ui/separator"
import { useParams, useRouter } from 'next/navigation'
import Lottie from "lottie-react";
import checkComplatedAnimation from "@/public/lotties/lotties-check-completed.json"
import spinnerLoadingAnimation from "@/public/lotties/lotties-spinner-loading.json"
import { Button } from "@/components/ui/button"
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { useTranslation } from 'react-i18next'
import axios from "axios"


const lottiesStyle = { width: 200, height: 200 }

const SpinnerLoading = () => {
    return (
        <div className="flex flex-col w-full min-h-[80vh] h-full overflow-y-auto items-center justify-center gap-4">
            <Lottie animationData={spinnerLoadingAnimation} style={lottiesStyle} loop={true} />
        </div>
    )
}

const fetchBooking = async (id) => {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/booking/${id}`);
    return response.data;
}

export default function Page() {

    const params = useParams()
    const router = useRouter()
    const lottieRef = useRef();

    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;

    const handleBookingDetail = (e) => {
        e.preventDefault();
        if (!params?.booking_id) return;
        router.replace(`/history/${params.booking_id}`)
    }

    const handleHome = (e) => {
        e.preventDefault();
        router.replace(`/`)
    }

    useEffect(() => {
        console.log("params", params)
    }, [])

    const { isPending, isError, data, error } = useQuery({
        queryKey: ['booking-completed'],
        queryFn: () => fetchBooking(params?.booking_id),
        enabled: !!params?.booking_id
    })

    if (isPending) {
        return <SpinnerLoading />
    }

    if (isError) {
        return <span>Error: {error.message}</span>
    }

    // if (data) {
    //     console.log("completed data", data);
    // }

    // if (data && data.data && 
    //     (data.data.booking_status.code !== 'PENDING' || 
    //     data.data.booking_status.code !== 'CONFIRMED' ||
    //     data.data.payment_status.code !== 'AWAITING-PAYMENT' || 
    //     data.data.payment_status.code !== 'PAID')) {
    //     // router.replace(`/`)
    //     console.log("completed data", data);
    //     return (<></>)
    // }

    if (data && data.data && 
        (data.data.booking_status.code === 'PENDING' || data.data.booking_status.code === 'CONFIRMED') &&
        (data.data.payment_status.code === 'AWAITING-PAYMENT-PROOF' || data.data.payment_status.code === 'PAID')
    ) {
        return (
            <>
                <div className="flex flex-col w-full min-h-[80vh] h-full overflow-y-auto items-center justify-center gap-4">
                    <Lottie lottieRef={lottieRef} animationData={checkComplatedAnimation} style={lottiesStyle} loop={false} />
    
                    <div className="text-2xl font-semibold">Your booking is completed</div>
                    {data.data.booking_no && <div className="font-thin">Booking no: <span className="font-medium">{data.data.booking_no}</span></div>}
    
                    <div className="mt-8 w-full flex flex-col justify-center px-12 lg:max-w-sm gap-4">
                        <Button type="button" variant="outline" className="w-full" onClick={handleHome}>
                            กลับหน้าหลัก
                        </Button>
                        <Button type="button" variant="" className="w-full" onClick={handleBookingDetail}>
                            ดูรายละเอียด
                        </Button>
                    </div>
    
                </div>
            </>
        )

    } else {
        router.replace(`/`)
        return (<></>)

    }
}

// export default function Page() {

//     return (
//         <>
//             <div className="flex flex-col items-center justify-center h-auto">

//                 <div class="max-w-sm w-full mx-auto bg-white rounded-xl shadow-md overflow-hidden mt-20">

//                     <div class="bg-blue-800 text-white p-4">
//                         <div class="flex items-center">
//                             <svg class="w-6 h-6 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
//                                 <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v1m6 11h2m-6 0h-2v4m0-11v3m0 0h.01M12 12h4.01M16 20h4M4 12h4m12 0h.01M5 8h2a1 1 0 001-1V5a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1zm12 0h2a1 1 0 001-1V5a1 1 0 00-1-1h-2a1 1 0 00-1 1v2a1 1 0 001 1zM5 20h2a1 1 0 001-1v-2a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1z"></path>
//                             </svg>
//                             <span class="font-bold text-lg">THAI QR PAYMENT</span>
//                         </div>
//                     </div>

//                     <div class="p-4">
//                         <div class="text-center my-6">
//                             <Image src={PromptpayLogo} alt="QR Code" width={80} height={60} className="mx-auto my-6" />
//                         </div>
//                         <div class="flex justify-center items-center text-center mb-4">
//                             <div className="w-60 h-60 border">QR</div>
//                         </div>

//                         <div className=" mb-4 flex flex-col gap-2 my-6 items-center">
//                             <p className="text-gray-700 text-lg font-semibold">บริษัท ไวด์ลี่เน็กท์ จำกัด</p>
//                             <p className="text-gray-500">0105547032653</p>
//                         </div>

//                         <div className="my-4 px-6">
//                             <Separator />
//                         </div>

//                         <div className="flex flex-col my-6 px-6">
//                         <div className="text-xs  text-slate-400">จำนวนเงิน</div>
//                         <div className="flex flex-row gap-2 items-center">
//                             <div className="text-md">600.00</div>
//                             <div className="text-xs text-slate-400">THB</div>
//                         </div>
//                     </div>
//                     </div>
//                 </div>

//             </div>

//         </>
//     )
// }