"use client"

import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"
import { Button } from "@/components/ui/button"
import { Separator } from "@/components/ui/separator"
import Link from 'next/link'
import { useEffect } from "react"
import { useParams, useRouter } from 'next/navigation'
import { Skeleton } from "@/components/ui/skeleton"
import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import { useTranslation } from 'react-i18next'
import { userStore } from '@/app/(store)/auth-store';
import VerifyMember from "@/app/(components)/member-verify"
import { cartStore } from '@/app/(store)/cart-store'
import parse from 'html-react-parser'
import { CalendarDays, ChevronRight, ChevronLeft, MapPin, Landmark, Banknote, X } from 'lucide-react';

export default function Page() {
    const params = useParams()
    const router = useRouter()
    const userSession = userStore((state) => state?.userSession)
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;

    const cart = cartStore((state) => state.cart)
    const setCart = cartStore((state) => state.setCart)
    const removeCart = cartStore((state) => state.removeCart)

    useEffect(() => {
        console.log('service detail use effect', params);
        console.log('service detail use effect cart', cart);
        console.log('service detail use effect userSession', userSession);

    }, [])



    const fetchService = async () => {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/service/${params.slug}`);
        return response.data;
    }

    const { isPending, isError, data, error } = useQuery({
        queryKey: ['service'],
        queryFn: fetchService,
    })

    console.log("service detail ", data);

    if (isError) {
        console.log("service detail error ", error)
    }

    const submit = async () => {
        console.log('submit', data);
        if (!data || !data?.data || !data?.success) return

        const service = data.data;

        removeCart()
        //default cart
        setCart({
            members_id: userSession.id,
            services_id: service.services_id,
            services_code: service.services_code,
            category_code: service.category_code,
            services_name_th: service.services_name_th,
            services_name_en: service.services_name_en,
            title_en: service.title_en,
            title_th: service.title_th,
            detail_en: service.detail_en,
            detail_th: service.detail_th,
            price: service.price,
            times: service.times,
            hour_per_time: service.hour_per_time,
            staff_per_time: service.staff_per_time,
            payment_method_id: '744dc10f-690b-11ef-b015-560004ee4748', //QR

        })
        router.push(`/service/${params.slug}/checkout`)
    }

    const handleContract = async (e) => {
        if (liff.isInClient() === true) {

            if (liff.getContext().type !== 'none') {

                await liff.sendMessages([
                    {
                        type: "text",
                        text: `ขอใบเสนอราคา ${data?.data?.title_th}`
                    }
                ])
            }

            liff.closeWindow()

        }
    }

    const onBack = () => {
        router.back();
    }

    return (
        <>
            <div className=" flex flex-col lg:flex-row bg-slate-100 min-h-screen">
                <div className="w-full lg:ml-8 px-0 h-full mb-28 sm:mb-28 lg:mb-10">
                    {isPending ?
                        <Skeleton className="p-4 h-14 w-[350px]" /> :
                        <>
                            <div className="flex flex-row justify-start items-center">
                                <div onClick={onBack} className="flex  w-10 h-10  ml-2 items-center justify-center drop-shadow-2xl cursor-pointer">
                                    <ChevronLeft strokeWidth={2} />
                                </div>
                                <div className="py-4 px-2">{currentLocale === "th" ? data?.data?.services_name_th : data?.data?.services_name_en}</div>
                            </div>
                        </>
                    }

                    <div className="p-4 bg-white">
                        <div className="flex flex-row justify-between">
                            <div className="flex flex-col">
                                {isPending ?
                                    <>
                                        <Skeleton className="h-3 w-[250px]" />
                                        <Skeleton className="h-3 w-[250px] mt-2" />
                                    </> :
                                    <>
                                        <div className="text-sm text-slate-400">{currentLocale === "th" ? data?.data?.services_name_th : data?.data?.services_name_en}</div>
                                        {
                                            !data?.data?.price ?
                                                <div className="text-sm font-semibold">ติดต่อขอใบเสนอราคา</div> :
                                                <div className="text-sm font-semibold">฿{data?.data?.price} / {data?.data?.staff_per_time} {t("person")}</div>
                                        }

                                    </>}

                            </div>
                        </div>

                        {isPending ?
                            <Skeleton className="my-6 h-3 w-[350px]" /> :
                            <div className="my-6 text-sm">{currentLocale === "th" ? data?.data?.title_th : data?.data?.title_en}</div>
                        }

                        {isPending ?
                            <div className="flex flex-col gap-2">
                                <Skeleton className="h-3 w-full" />
                                <Skeleton className="h-3 w-full" />
                                <Skeleton className="h-3 w-full" />
                                <Skeleton className="h-3 w-full" />
                                <Skeleton className="h-3 w-full" />
                            </div> :
                            <div className="text-sm leading-loose">{currentLocale === "th" ? parse(data?.data?.detail_th) : parse(data?.data?.detail_en)}</div>
                        }
                    </div>

                </div>

                {/* mobile */}
                <div className="fixed bottom-0 z-50 h-[100px] bg-white w-full p-6 lg:hidden">
                    {userSession?.verified === true ?
                        (!data?.data?.price ?
                            <Button className="w-full bg-green-600 h-[50px] hover:bg-green-600/90" disabled={isPending} onClick={handleContract}>
                                <div className="flex flex-row justify-center w-full">
                                    <div>ติดต่อขอใบเสนอราคา</div>
                                </div>
                            </Button> :
                            <Button className="w-full bg-green-600 h-[50px] hover:bg-green-600/90" disabled={isPending} onClick={submit}>
                                <div className="flex flex-row justify-between w-full">
                                    <div className="flex flex-row gap-2">
                                        <div className="w-5 h-5 rounded-full bg-white flex justify-center items-center text-green-600">1</div>
                                        <div>{t('total')}</div>
                                    </div>
                                    <div>฿{isPending ? '' : data?.data?.price}</div>
                                </div>
                            </Button>) :
                        <VerifyMember>
                            <Button className="w-full bg-green-600 h-[50px] hover:bg-green-600/90" disabled={isPending}>
                                <div className="flex flex-row justify-between w-full">
                                    <div className="flex flex-row gap-2">
                                        <div className="w-5 h-5 rounded-full bg-white flex justify-center items-center text-green-600">1</div>
                                        <div>{t('total')}</div>
                                    </div>
                                    <div>฿{isPending ? '' : data?.data?.price}</div>
                                </div>
                            </Button>
                        </VerifyMember>}
                </div>

                {/* desktop */}
                <div className="hidden lg:flex w-2/3 bg-slate-100">
                    <div className="sticky top-28 bg-white mt-14 w-full h-fit p-4 lg:mx-8">
                        {isPending ?
                            <Skeleton className="h-4 w-full" /> :
                            <div className="font-semibold text-lg">{currentLocale === "th" ? data?.data?.services_name_th : data.data.services_name_en}</div>}


                        {isPending ?
                            <Skeleton className="my-6 h-3 w-full" /> :
                            <>
                                <div className="flex flex-row justify-between my-6 text-sm text-slate-500">
                                    <div>{currentLocale === "th" ? data?.data?.services_name_th : data?.data?.services_name_en}</div>
                                    <div>
                                        {!data?.data?.price ? 'ขอใบเสนอราคา' : `฿${data?.data?.price}`}
                                    </div>
                                </div>
                            </>}

                        <Separator className="my-6" />

                        {isPending ?
                            <Skeleton className="my-6 h-4 w-full" /> :
                            <div className="flex flex-row justify-between text-sm my-6 text-slate-500 font-semibold">
                                <div>{t('total')}</div>
                                <div>
                                    {!data?.data?.price ? 'ขอใบเสนอราคา' : `฿${data?.data?.price}`}
                                </div>
                            </div>}

                        {userSession?.verified === true ?
                            (!data?.data?.price ?
                                <Button className="w-full bg-green-600 hover:bg-green-600/90" disabled={isPending} onClick={handleContract}>
                                    ติดต่อขอใบเสนอราคา
                                </Button> :
                                <Button className="w-full bg-green-600 hover:bg-green-600/90" disabled={isPending} onClick={submit}>
                                    {t("btn_continue")}
                                </Button>
                            ) :
                            <VerifyMember>
                                <Button className="w-full bg-green-600 hover:bg-green-600/90" disabled={isPending}>
                                    {t("btn_continue")}
                                </Button>
                            </VerifyMember>}
                    </div>
                </div>
            </div>
        </>
    )
}

function ServiceCardSkeleton() {

    return (
        <></>
    )
}