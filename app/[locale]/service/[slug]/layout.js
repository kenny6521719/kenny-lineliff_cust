

import { Button } from "@/components/ui/button"
import { Separator } from "@/components/ui/separator"
import Link from 'next/link'
import TranslationsProvider from '@/app/providers/TranslationProvider'
import initTranslations from '@/app/i18n'

export default async function ServiceLayout({ children, params: { locale } }) {
    const i18nNamespace = ['service'];
    const { resources } = await initTranslations(locale, i18nNamespace);
    return (
        <>
            <TranslationsProvider
                namespaces={i18nNamespace}
                locale={locale}
                resources={resources}>
                <div className="flex flex-col">
                    <div className="mb-30">{children}</div>
                </div>
            </TranslationsProvider>
        </>
    );
}