"use client";
import React, {
    createContext,
    useContext,
    useRef,
    useState,
    forwardRef,
    useEffect,
    useCallback,
} from "react";
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
    DialogFooter,
    DialogClose,
    DialogOverlay,
} from "@/components/ui/dialog";
import {
    Drawer,
    DrawerClose,
    DrawerContent,
    DrawerDescription,
    DrawerFooter,
    DrawerHeader,
    DrawerTitle,
    DrawerTrigger,
    DrawerOverlay,
    DrawerPortal,
} from "@/components/ui/drawer";
import { CalendarDays, ChevronRight, MapPin, Plus, LocateFixed, Pin } from "lucide-react";
import { Label } from "@/components/ui/label";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Button } from "@/components/ui/button";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { useMediaQuery } from "@/app/hooks/use-media-query";
import { cn } from "@/lib/utils";
import { userStore } from "@/app/(store)/auth-store";
import axios from "axios";
import { Skeleton } from "@/components/ui/skeleton";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { cartStore } from "@/app/(store)/cart-store";
import { GoogleMap, useJsApiLoader, Marker } from "@react-google-maps/api";
import {
    Tooltip,
    TooltipContent,
    TooltipProvider,
    TooltipTrigger,
} from "@/components/ui/tooltip"
import { useTranslation } from 'react-i18next'
import { Textarea } from "@/components/ui/textarea"
import MapSelectionPage from "@/app/(components)/map-selection"
import { addressHandlerStore } from '@/app/(store)/address-store'
import { useToast } from "@/components/ui/use-toast"


const updateAddress = async (values) => {
    const response = await axios.post(
        `${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/update-address`,
        values,
        {
            headers: {
                "Content-Type": "application/json",
            },
        }
    );
    return response.data;
};

export default function MemberAddress() {
    const [open, setOpen] = useState(false);
    const [addressSelected, setAddressSelected] = useState({});
    const isDesktop = useMediaQuery("(min-width: 768px)");


    const { t, i18n } = useTranslation();

    const cart = cartStore((state) => state.cart);
    const setCart = cartStore((state) => state.setCart);

    const onSubmit = async () => {
        console.log("onSubmit", addressSelected);
        setCart({
            address_name: addressSelected?.address_name,
            address_full: addressSelected?.full_address,
            address_contact_name: addressSelected?.contact_name,
            address_contact_phone: addressSelected?.contact_phone,
            address_coordinate_x: addressSelected?.address_coordinate_x,
            address_coordinate_y: addressSelected?.address_coordinate_y,
        });
        setOpen(false);
    };

    if (isDesktop) {
        return (
            <Dialog open={open} onOpenChange={setOpen}>
                <DialogTrigger asChild>
                    {cart?.address_name ? (
                        <div className="flex flex-row mt-4 px-4 py-2 w-full border cursor-pointer justify-between hover:border-slate-300">
                            <div className="flex flex-row gap-4">
                                <MapPin size={20} strokeWidth={2} />
                                <div className="flex flex-col">
                                    <div>{cart?.address_name}</div>
                                    <div className="text-xs mt-2 text-slate-500">
                                        {cart?.address_full}
                                    </div>
                                    <div className="text-xs mt-2 text-slate-500">
                                        {cart?.address_contact_name} {cart?.address_contact_phone}
                                    </div>
                                </div>
                            </div>
                            <div className="flex justify-center items-center">
                                <ChevronRight size={20} strokeWidth={2} />
                            </div>
                        </div>
                    ) : (
                        <div className="flex flex-row justify-between border p-4 cursor-pointer mt-4 shadow-md">
                            <div className="flex flex-row gap-4 items-center">
                                <Plus size={20} strokeWidth={2} />
                                <div className="text-sm text-gray-500">{t("dialog_locaton.btn_add")}</div>
                            </div>
                        </div>
                    )}
                </DialogTrigger>
                <DialogContent className="flex flex-col p-4">
                    <DialogTitle>{t("dialog_locaton.header")}</DialogTitle>
                    <DialogDescription></DialogDescription>
                    <AddressList
                        addressSelected={addressSelected}
                        setAddressSelected={setAddressSelected}
                    />
                    <DialogFooter className="sm:justify-end gap-4 mt-6">
                        <DialogClose asChild>
                            <Button type="button" variant="secondary">
                                {t("dialog_locaton.btn_close")}
                            </Button>
                        </DialogClose>
                        {/* <Button type="button" variant="secondary" onClick={() => setOpen(false)}>
                                Close
                            </Button> */}
                        <Button type="button" variant="" onClick={onSubmit}>
                            {t("dialog_locaton.btn_confirm")}
                        </Button>
                    </DialogFooter>
                </DialogContent>
            </Dialog>
        );
    }

    return (
        <Drawer open={open} onOpenChange={setOpen} shouldScaleBackground>
            <DrawerTrigger asChild>
                {cart?.address_name ? (
                    <div className="flex flex-row mt-4 px-4 py-2 w-full border cursor-pointer justify-between hover:border-slate-300">
                        <div className="flex flex-row gap-4">
                            <MapPin size={20} strokeWidth={2} />
                            <div className="flex flex-col">
                                <div>{cart?.address_name}</div>
                                <div className="text-xs mt-2 text-slate-500">
                                    {cart?.address_full}
                                </div>
                                <div className="text-xs mt-2 text-slate-500">
                                    {cart?.address_contact_name} {cart?.address_contact_phone}
                                </div>
                            </div>
                        </div>
                        <div className="flex justify-center items-center">
                            <ChevronRight size={20} strokeWidth={3} />
                        </div>
                    </div>
                ) : (
                    <div className="flex flex-row justify-between border p-4 cursor-pointer mt-4 shadow-md">
                        <div className="flex flex-row gap-4 items-center">
                            <Plus size={20} strokeWidth={3} />
                            <div className="text-sm text-slate-500">{t("dialog_locaton.btn_add")}</div>
                        </div>
                    </div>
                )}
            </DrawerTrigger>
            <DrawerContent className="flex flex-col rounded-t-[10px] h-[96%] mt-24 p-4">
                <DrawerHeader className="text-left mt-6">
                    <DrawerTitle>{t("dialog_locaton.header")}</DrawerTitle>
                    <DrawerDescription></DrawerDescription>
                </DrawerHeader>
                <AddressList
                    addressSelected={addressSelected}
                    setAddressSelected={setAddressSelected}
                />
                <DrawerFooter className="pt-2">
                    <Button type="button" variant="" onClick={onSubmit}>
                        {t("dialog_locaton.btn_confirm")}
                    </Button>
                    <DrawerClose asChild>
                        <Button variant="outline">{t("dialog_locaton.btn_cancel")}</Button>
                    </DrawerClose>
                </DrawerFooter>
            </DrawerContent>
        </Drawer>
    );
}

function AddressList({ addressSelected, setAddressSelected }) {
    const { t, i18n } = useTranslation();
    const userSession = userStore((state) => state?.userSession);

    const addressHendler = (value) => {
        setAddressSelected(value);
    };

    const fetchAddressList = async () => {
        const response = await axios.get(
            `${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/address-member/${userSession.id}`
        );
        console.log("fetchAddressList", response);
        return response.data;
    };

    const { isPending, isError, data, error } = useQuery({
        queryKey: ["addressList"],
        queryFn: fetchAddressList,
        refetchOnWindowFocus: false,
        enabled: !!userSession?.id,
    });

    if (isPending || isError) {
        if (isError) console.log(error.message);
        return <AddressSkeletonCard />;
    }

    return (
        <>
            <div className="flex flex-col w-full mt-4">
                <div className="flex flex-col">
                    <RadioGroup
                        defaultValue=""
                        className="flex flex-col gap-4 w-full"
                        aria-label="Address list"
                        onValueChange={addressHendler}
                    >
                        {data?.data?.map((addr, idx) => (
                            <div
                                key={idx}
                                className='w-full bg-white border shadow-md cursor-pointer p-4 hover:border-green-300 [&:has([data-state=checked])]:border-2 [&:has([data-state=checked])]:border-green-500 [&:has([data-state=checked])]:dark:border-green-500'
                            >
                                <RadioGroupItem
                                    className="peer sr-only w-full"
                                    id={idx}
                                    value={addr}
                                />
                                <Label htmlFor={idx} className="block w-full">
                                    <div className="flex flex-row w-full cursor-pointer justify-between">
                                        <div className="flex flex-row gap-4 flex-grow min-w-0">
                                            <div className="w-5 h-5 flex-shrink-0 flex items-center justify-center">
                                                <MapPin size={20} strokeWidth={2} />
                                            </div>
                                            <div className="flex flex-col flex-grow min-w-0">
                                                <div className="font-normal">{addr.address_name}</div>
                                                <div className="text-xs font-light mt-2 text-slate-500 overflow-hidden">
                                                    <p className="truncate">{addr.full_address}</p>
                                                </div>
                                                <div className="text-xs mt-2 text-slate-500 font-light">
                                                    {addr.contact_name} {addr.contact_phone}
                                                </div>
                                            </div>
                                        </div>
                                        <MemberAddressInfo addr={addr}>
                                            <div className="flex-shrink-0 flex justify-center items-center ml-2">
                                                <ChevronRight size={20} strokeWidth={3} />
                                            </div>
                                        </MemberAddressInfo>
                                    </div>
                                </Label>
                            </div>
                        ))}
                    </RadioGroup>
                </div>

                {/* new address */}
                <MemberAddressInfo addr={null}>
                    <div className="flex flex-row justify-between border p-4 cursor-pointer mt-4 shadow-md">
                        <div className="flex flex-row gap-2 items-center">
                            <Plus size={20} strokeWidth={3} />
                            <div>{t("dialog_locaton.btn_new_address")}</div>
                        </div>
                        <div className="flex justify-center items-center">
                            <ChevronRight size={20} strokeWidth={3} />
                        </div>
                    </div>
                </MemberAddressInfo>
            </div>
        </>
    );
}


function MemberAddressInfo({ children, addr }) {
    const { toast } = useToast()
    const [libraries] = useState(['marker', 'geocoding']);
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;

    // const center = { lat: 13.7563, lng: 100.5018 };
    const [dialogOpen, setDialogOpen] = useState(false);
    const queryClient = useQueryClient();
    const userSession = userStore((state) => state?.userSession);

    const addressStore = addressHandlerStore((state) => state.address);
    const setAddressStore = addressHandlerStore((state) => state.setAddress);
    const clearAddressStore = addressHandlerStore((state) => state.removeAddress);

    const formSchema = z.object({
        address_id: z.string().nullable(),
        member_id: z.string(),
        address_name: z.string().min(4, {
            message: currentLocale === "th" ? "กรุณาระบบข้อมูล" : "Please specify a address name",
        }),
        contact_name: z.string().min(1, { message: currentLocale === "th" ? "กรุณาระบบข้อมูล" : "Please specify a contact name" }),
        contact_phone: z
            .string()
            .min(10, { message: currentLocale === "th" ? "เบอร์มือถือควรมี 10 หลัก" : "Phone number must have 10 digits" })
            .max(10, { message: currentLocale === "th" ? "เบอร์มือถือควรมี 10 หลัก" : "Phone number must have 10 digits" }),
    });

    const initialValues = {
        address_id: addr?.id || null,
        member_id: addr?.members_id || userSession.id,
        address_name: addr?.address_name || "",
        contact_name: addr?.contact_name || "",
        contact_phone: addr?.contact_phone || "",
    };

    const form = useForm({
        resolver: zodResolver(formSchema),
        defaultValues: initialValues,
    });

    const mutation = useMutation({
        mutationFn: updateAddress,
        retry: false,
        onSuccess: (res) => {
            console.log("mutation success", res);
            if (res?.success === true) {
                queryClient.invalidateQueries("addressList");
                toast({
                    description: currentLocale === "th" ? res?.message : res?.message_en,
                })
            }
            
        },
        onError: (err) => {
            console.log("mutation error", err);
            toast({
                description: currentLocale === "th" ? 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง' : 'Uh oh! Something went wrong. please try again',
            })
        },
        onSettled: () => {
            clearAddressStore();
            setDialogOpen(false);
        }
    });


    const onSubmit = (values) => {
        // Do something with the form values.
        // ✅ This will be type-safe and validated.
        const result = { ...values, 
            ...addressStore, 
            address_coordinate_x: `${addressStore.address_coordinate_x}`,
            address_coordinate_y: `${addressStore.address_coordinate_y}`,
        }
        mutation.mutate(result);
    };

    return (
        <>
            <Dialog open={dialogOpen} onOpenChange={setDialogOpen}>
                <DialogTrigger asChild>{children}</DialogTrigger>
                <DialogContent className="flex flex-col max-h-[90%] overflow-y-auto">
                    <DialogHeader>
                        <DialogTitle className="">{t("dialog_address_info.header")}</DialogTitle>
                        <DialogDescription></DialogDescription>
                    </DialogHeader>

                    <Form {...form}>
                        <form onSubmit={form.handleSubmit(onSubmit)} className="h-full">
                            <div className="flex flex-col mt-4  gap-2">
                                <FormField
                                    control={form.control}
                                    name="address_name"
                                    render={({ field }) => (
                                        <FormItem>
                                            <FormLabel>
                                                {t("dialog_address_info.name")}<span className="text-red-500"> *</span>
                                            </FormLabel>
                                            <FormControl>
                                                <Input
                                                    placeholder={t("dialog_address_info.name_placeholder")}
                                                    {...field}
                                                    className=""
                                                />
                                            </FormControl>
                                            <FormDescription></FormDescription>
                                            <FormMessage />
                                        </FormItem>
                                    )}
                                />
                                <div className="my-2"></div>
                                <FormField
                                    control={form.control}
                                    name="contact_name"
                                    render={({ field }) => (
                                        <FormItem>
                                            <FormLabel>
                                                {t("dialog_address_info.contact_info")}<span className="text-red-500"> *</span>
                                            </FormLabel>
                                            <FormControl>
                                                <Input placeholder={t("dialog_address_info.contact_name")} {...field} className="" />
                                            </FormControl>
                                            <FormMessage />
                                        </FormItem>
                                    )}
                                />
                                <FormField
                                    control={form.control}
                                    name="contact_phone"
                                    render={({ field }) => (
                                        <FormItem>
                                            <FormLabel></FormLabel>
                                            <FormControl>
                                                <Input
                                                    placeholder={t("dialog_address_info.contact_phone")}
                                                    {...field}
                                                    className=""
                                                />
                                            </FormControl>
                                            <FormMessage />
                                        </FormItem>
                                    )}
                                />

                                <div className="flex flex-col mb-2">
                                    <div className="mt-6 text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70">
                                        {t("dialog_address_info.map_address_info")}
                                    </div>

                                    {/* new page for google map */}
                                    <MapSelectionPage address={addr} />
                                </div>

                                <FormField
                                    control={form.control}
                                    name="detail"
                                    render={({ field }) => (
                                        <FormItem>
                                            <FormLabel>{t("dialog_address_info.map_address_note")}</FormLabel>
                                            <FormControl>
                                                <Input placeholder="" {...field} className="" />
                                            </FormControl>
                                            <FormMessage />
                                        </FormItem>
                                    )}
                                />
                            </div>

                            <div className="flex sm:justify-end gap-4 w-full mt-6">
                                <Button
                                    type="submit"
                                    className="w-full sm:w-auto"
                                    disabled={mutation.isPending}
                                    onClick={() => {
                                        console.log("Form errors:", form.formState.errors);
                                    }}
                                >
                                    {mutation.isPending ? `${t("dialog_address_info.btn_confirm")}...` : `${t("dialog_address_info.btn_confirm")}`}
                                </Button>
                            </div>
                        </form>
                    </Form>
                </DialogContent>
            </Dialog>
        </>
    );
}

function AddressSkeletonCard() {
    return (
        <>
            <div className="flex flex-col w-full gap-4">
                {[...Array(3)].map((_, index) => (
                    <div key={index} className="bg-white border shadow-md p-4 ">
                        <div className="flex flex-row w-full justify-between">
                            <div className="flex flex-row gap-4 w-full">
                                <Skeleton className="h-6 w-6 rounded-full" />
                                <div className="flex flex-col gap-2 w-full">
                                    <Skeleton className="h-3 w-[150px]" />
                                    <Skeleton className="h-2 w-full" />
                                    <Skeleton className="h-2 w-full" />
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );
}


