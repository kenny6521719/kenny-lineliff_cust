"use client"

import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
    DialogFooter,
    DialogClose
} from "@/components/ui/dialog"
import {
    Drawer,
    DrawerClose,
    DrawerContent,
    DrawerDescription,
    DrawerFooter,
    DrawerHeader,
    DrawerTitle,
    DrawerTrigger,
    DrawerOverlay,
    DrawerPortal,
} from "@/components/ui/drawer"
import { Button } from "@/components/ui/button"
import { Separator } from "@/components/ui/separator"
import { MapPin, Phone, ListOrdered } from 'lucide-react';
import { cartStore } from '@/app/(store)/cart-store'
import { useMediaQuery } from "@/app/hooks/use-media-query"
import React, { useState } from 'react';
import { formatNumberWithThousandSeparator } from "@/lib/numberFormat";
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import axios from 'axios';
import { useRouter } from 'next/navigation'
import { useTranslation } from 'react-i18next'
import { useToast } from "@/components/ui/use-toast"

const craeteBooking = async (values) => {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/create-booking`, values, {
        headers: {
            'Content-Type': 'application/json'
        }
    });
    return response.data;
}

const sendFlexMessageNewbookingRequest = async (booking_id) => {
    try {
        const response = await axios.post(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/notify/booking/${booking_id}`, {}, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    } catch (error) {
        console.error(error);
    }

}


const sendNotiToAdminRequest = async (booking_id) => {
    try {
        const response = await axios.post(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/notify/booking/${booking_id}/admin`, {}, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    } catch (error) {
        console.error(error);
    }

}
const newbookingNoti = async (booking_id) => {
    console.log("newbookingNoti woking..")
    const flexResponse = await sendFlexMessageNewbookingRequest(booking_id);
    const notiResponse = await sendNotiToAdminRequest(booking_id);
}


export default function ConfirmBooking() {
    const router = useRouter()
    const [open, setOpen] = useState(false)
    const isDesktop = useMediaQuery("(min-width: 768px)")
    const cart = cartStore((state) => state.cart)
    const removeCart = cartStore((state) => state.removeCart)
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;
    const { toast } = useToast()

    const mutation = useMutation({
        mutationFn: craeteBooking,
        retry: false,
        onSuccess: (res) => {
            console.log('mutation success', res);

            if (res?.data && res.success === true) {
                removeCart()
                const noti = newbookingNoti(res.data);
                router.replace(`/payment/${res.data}`)
                return
            }

            toast({
                description: res?.message || "There was a problem with your request. please try again",
                duration: 5000,
            })
            return
        },
        onError: (err) => {
            console.log('mutation error', err);
            toast({
                description: err?.message || "There was a problem with your request. please try again",
                duration: 5000,
            })
        },
        onSettled: () => {
            setOpen(false)
        }
    })

    const onSubmit = (e) => {
        e.preventDefault();
        console.log('booking ', cart)
        mutation.mutate(cart);
    }

    const confirmBookingContent = () => (
        <>
            <div className="flex flex-col w-full px-4">
                <div className='flex flex-row gap-3 w-full'>
                    <div>
                        <ListOrdered size={20} strokeWidth={2} />
                    </div>
                    <div className='flex flex-col  w-full pr-4'>
                        <div className='font-semibold text-xs'>{currentLocale === "th" ? cart?.services_name_th : cart?.services_name_en}</div>
                    </div>
                </div>
                <div className='flex flex-row gap-3 w-full mt-4 pr-8'>
                    <div>
                        <MapPin size={20} strokeWidth={2} />
                    </div>
                    <div className='flex flex-col  w-full'>
                        <div className='font-semibold text-xs'>{cart?.address_name}</div>
                        <p className='font-thin truncate text-xs'>{cart?.address_full}</p>
                    </div>
                </div>
                <div className='flex flex-row gap-3 w-full mt-4'>
                    <div>
                        <Phone size={20} strokeWidth={2} />
                    </div>
                    <div className='flex flex-col  w-full pr-4'>
                        <div className='font-semibold text-xs'>{cart?.address_contact_name} {cart?.address_contact_phone}</div>
                    </div>
                </div>

                <Separator className="my-4" />

                <div className='flex flex-row justify-between items-end'>
                    <div className='text-xl text-green-600 font-bold'>{t("dialog_confirm_booking.total")}</div>

                    <div className='flex flex-row items-end gap-4'>
                        <div className='text-xl font-bold text-orange-600'>
                            {cart?.net_price ? formatNumberWithThousandSeparator(cart?.net_price, 0) : formatNumberWithThousandSeparator(cart?.price, 0)}
                        </div>
                        <div className='text-md text-black'>{t("dialog_confirm_booking.currency")}</div>
                    </div>
                </div>

                <Separator className="my-4" />
            </div>
        </>
    )

    if (isDesktop) {
        return (
            <Dialog open={open} onOpenChange={setOpen}>
                <DialogTrigger asChild>
                    <div className="p-4 mt-4 mb-20">
                        <Button className="w-full h-[50px] text-lg bg-green-600 hover:bg-green-600/90" >
                            {t("btn_booking")}
                        </Button>
                    </div>
                </DialogTrigger>
                <DialogContent className="flex flex-col">
                    <DialogTitle>{t("dialog_confirm_booking.header")}</DialogTitle>
                    <DialogDescription></DialogDescription>
                    {confirmBookingContent()}
                    <DialogFooter className="sm:justify-end gap-4 mt-4">
                        <DialogClose asChild>
                            <Button type="button" variant="secondary">
                                {t("dialog_confirm_booking.btn_cancel")}
                            </Button>
                        </DialogClose>

                        <Button type="button" variant="" onClick={onSubmit} disabled={mutation.isPending}>
                            {t("dialog_confirm_booking.btn_confirm")}
                        </Button>
                    </DialogFooter>
                </DialogContent>
            </Dialog>
        )
    }
    return (
        <Drawer open={open} onOpenChange={setOpen} shouldScaleBackground>
            <DrawerTrigger asChild>
                <div className="p-4 mt-4 mb-20">
                    <Button className="w-full h-[50px] text-lg bg-green-600 hover:bg-green-600/90">
                        {t("btn_booking")}
                    </Button>
                </div>
            </DrawerTrigger>
            <DrawerContent className="flex flex-col rounded-t-[10px] h-auto mt-24 p-1.5 w-full">
                <DrawerHeader className="flex flex-row justify-between items-center">
                    <DrawerTitle className="text-xl">{t("dialog_confirm_booking.header")}</DrawerTitle>
                    <div onClick={() => setOpen(false)} className='border border-transparent rounded-md px-1.5 py-1 bg-slate-200/60 text-md font-semibold cursor-pointer'>
                        {t("dialog_confirm_booking.btn_cancel")}
                    </div>
                </DrawerHeader>
                <DrawerDescription></DrawerDescription>

                {confirmBookingContent()}

                <DrawerFooter className="pt-2 mb-6 mt-4">
                    <Button type="button" variant="" onClick={onSubmit} disabled={mutation.isPending}>
                        {t("dialog_confirm_booking.btn_confirm")}
                    </Button>
                </DrawerFooter>
            </DrawerContent>
        </Drawer>
    )
}
