"use client"

import React, { useState, useMemo, createContext, useContext, useEffect, useRef } from 'react';
import { CalendarDays, ChevronRight, MapPin, Landmark, Banknote, Plus } from 'lucide-react';
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
    DialogFooter,
    DialogClose
} from "@/components/ui/dialog"
import { Calendar } from "@/components/ui/calendar"
import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"
import { Button } from "@/components/ui/button"
import { cartStore } from '@/app/(store)/cart-store'
import { useForm } from "react-hook-form"
import { z } from "zod"
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form"
import { zodResolver } from "@hookform/resolvers/zod"
import { addHours } from "@/lib/dateUtils"
import { addDays, format } from "date-fns"
import { useTranslation } from 'react-i18next'
import { th, enUS } from "date-fns/locale";

export default function BookingDatesDialog() {

    const [open, setOpen] = useState(false);
    const [selectedDates, setSelectedDates] = useState([])
    const [selectedTime, setSelectedTime] = useState('')
    const [valid, setIsValid] = useState(false)

    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;

    const cart = cartStore((state) => state.cart)
    const setCart = cartStore((state) => state.setCart)

    const today = new Date()
    const minDateOpen = addDays(today, 1)

    useEffect(() => {
        console.log("date 2 init")
        return () => {
            console.log("cleaned up");
            setSelectedTime('')
            setSelectedDates([])
        }
    }, [open, currentLocale])

    useEffect(() => {
        if (selectedTime && selectedDates?.length > 0) {
            setIsValid(true);
        } else {
            setIsValid(false);
        }
    }, [selectedTime, selectedDates])

    const onSubmit = () => {
        if (!selectedTime || !selectedDates || selectedDates?.length === 0) return

        console.log("submit selectedDates", selectedDates)
        console.log("submit selectedTime", selectedTime)

        const times = cart?.times
        const result = []

        const date = [...selectedDates]
        const time = selectedTime;

        date?.forEach((date) => {
            result.push({
                id: null,
                booking_id: null,
                booking_date: format(date, 'yyyy-MM-dd'),
                booking_time_start: time,
                booking_time_finish: addHours(time, cart?.hour_per_time),
            })
        })


        // ถ้า result ยังไม่เท่ากับ times ให้เติมข้อมูลเปล่า
        while (result?.length < +times) {
            result.push({
                id: null,
                booking_id: null,
                booking_date: null,
                booking_time_start: null,
                booking_time_finish: null,
            });
        }

        console.log('onSubmit', result)

        setCart({ booking_item: [...result] })
        setOpen(false)
    }

    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild>
                <div className='flex flex-row justify-between border p-4 cursor-pointer mt-4 shadow-md'>
                    <div className='flex flex-row gap-4 items-center'>
                        <CalendarDays size={20} strokeWidth={2} />
                        <div className="text-sm text-gray-500">{t("dialog_date.btn_add")}</div>
                    </div>
                </div>
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>{t("dialog_date.header")}</DialogTitle>
                    <DialogDescription></DialogDescription>
                </DialogHeader>

                <form className='flex flex-col w-full'>
                    <div className="flex flex-row justify-between mb-4">
                        <div className='text-sm'>{t("dialog_date.working_date")}</div>
                        <div className='text-slate-500 text-sm'>({t("dialog_date.choose")} {cart?.times || 0} {t("dialog_date.times")})</div>
                    </div>

                    {
                        cart?.times > 1 ?
                            <Calendar
                                showOutsideDays={false}
                                locale={currentLocale === 'th' ? th : enUS}
                                mode="multiple"
                                selected={selectedDates}

                                onSelect={setSelectedDates}
                                disabled={{ before: minDateOpen }}
                                min={0}
                                max={cart?.times}
                                className="flex justify-center"
                                timeZone="Asia/Bangkok"
                            /> :
                            <Calendar
                                showOutsideDays={false}
                                locale={currentLocale === 'th' ? th : enUS}
                                mode="single"
                                selected={selectedDates}
                                onSelect={(date) => {
                                    console.log(date)
                                    const dateSelected = [];
                                    dateSelected.push(date)
                                    console.log(dateSelected)
                                    setSelectedDates(dateSelected)
                                }}
                                disabled={{ before: minDateOpen }}
                                className="flex justify-center"
                                timeZone="Asia/Bangkok"
                            />
                    }


                    <div className="flex flex-row justify-between mt-4">
                        <div className='text-sm'>{t("dialog_date.working_time")}</div>
                        <div className='text-slate-500 text-sm'>({cart?.hour_per_time || 0} {t("dialog_date.hours")})</div>
                    </div>

                    <div className="overflow-y-auto overflow-x-hidden mt-4 mb-8">
                        <Select value={selectedTime} onValueChange={setSelectedTime}>
                            <SelectTrigger className="w-full justify-between">
                                <SelectValue placeholder={t("dialog_date.dropdown_time_placeholder")}></SelectValue>
                            </SelectTrigger>
                            <SelectContent className="overflow-y-auto">
                                <SelectItem value="08:00">08:00</SelectItem>
                                <SelectItem value="08:30">08:30</SelectItem>
                                <SelectItem value="09:00">09:00</SelectItem>
                                <SelectItem value="09:30">09:30</SelectItem>
                                <SelectItem value="10:00">10:00</SelectItem>
                                <SelectItem value="10:30">10:30</SelectItem>
                                <SelectItem value="11:00">11:00</SelectItem>
                                <SelectItem value="11:30">11:30</SelectItem>
                                <SelectItem value="12:00">12:00</SelectItem>
                                <SelectItem value="12:30">12:30</SelectItem>
                                <SelectItem value="13:00">13:00</SelectItem>
                                <SelectItem value="13:30">13:30</SelectItem>
                                <SelectItem value="14:00">14:00</SelectItem>
                                <SelectItem value="14:30">14:30</SelectItem>
                                <SelectItem value="15:00">15:00</SelectItem>
                                <SelectItem value="15:30">15:30</SelectItem>
                                <SelectItem value="16:00">16:00</SelectItem>
                                <SelectItem value="16:30">16:30</SelectItem>
                                <SelectItem value="17:00">17:00</SelectItem>
                                <SelectItem value="17:30">17:30</SelectItem>
                                <SelectItem value="18:00">18:00</SelectItem>
                            </SelectContent>
                        </Select>
                    </div>

                </form>

                <DialogFooter className="sm:justify-end gap-4">
                    <DialogClose asChild>
                        <Button type="button" variant="secondary">
                            {t("dialog_date.btn_close")}
                        </Button>
                    </DialogClose>
                    <Button type="button" variant="" onClick={onSubmit}>
                        {t("dialog_date.btn_confirm")}
                    </Button>
                </DialogFooter>
            </DialogContent>
        </Dialog>


    );
}