"use client"

import { useEffect, useState } from "react";
import { addHours } from "@/lib/dateUtils"
import { cartStore } from '@/app/(store)/cart-store'
import { Calendar } from "@/components/ui/calendar"
import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"
import { Button } from "@/components/ui/button"
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
    DialogFooter,
    DialogClose
} from "@/components/ui/dialog"
import { CalendarDays, ChevronRight } from 'lucide-react';
import { addDays, format } from "date-fns"
import { useTranslation } from 'react-i18next'
import { th, enUS } from "date-fns/locale";

export default function BookingDateDialog({ item, index }) {

    const [open, setOpen] = useState(false);
    const [selectedDates, setSelectedDates] = useState(null)
    const [selectedTime, setSelectedTime] = useState('')
    const [valid, setIsValid] = useState(false)
    const [bookingDate, setBookingDate] = useState(null)

    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;

    const cart = cartStore((state) => state.cart)
    const setCart = cartStore((state) => state.setCart)

    const today = new Date();
    const minDateOpen = addDays(today, 1);
    const currentMonth = new Date(new Date().getFullYear, new Date().getMonth() + 1);

    const getDisabledDates = () => {
        return cart?.booking_item?.map(item => new Date(item.booking_date));
    };

    useEffect(() => {
        console.log("date 1 init")
        // console.log('BookingDateDialog', item);
        // console.log('BookingDateDialog index', index);
        console.log('BookingDateDialog cart', cart);

        setBookingDate({
            id: item.id || null,
            booking_id: item.booking_id || null,
            booking_date: item.booking_date || null,
            booking_time_start: item.booking_time_start || null,
            booking_time_finish: item.booking_time_finish || null,
        })

        setSelectedDates(new Date(item.booking_date) || null)
        setSelectedTime(item.booking_time_start || null)
    }, [open, currentLocale])

    useEffect(() => {
        if (selectedTime && selectedDates) {
            setIsValid(true);
        } else {
            setIsValid(false);
        }

        return () => {
            setIsValid(false);
        }
    }, [selectedTime, selectedDates])

    useEffect(() => {
        if (cart.booking_item && cart.booking_item[index]) {
            const item = cart.booking_item[index];
            setBookingDate({
                id: item.id || null,
                booking_id: item.booking_id || null,
                booking_date: item.booking_date || null,
                booking_time_start: item.booking_time_start || null,
                booking_time_finish: item.booking_time_finish || null,
            });
            setSelectedDates(item.booking_date ? new Date(item.booking_date) : null);
            setSelectedTime(item.booking_time_start || null);
        }
    }, [cart, index]);


    const onSubmit = () => {
        if (!selectedDates || !selectedTime) return


        const date = {
            id: bookingDate.id,
            booking_id: bookingDate.booking_id,
            booking_date: format(selectedDates, 'yyyy-MM-dd'),
            booking_time_start: selectedTime,
            booking_time_finish: addHours(selectedTime, cart?.hour_per_time),
        }

        const updatedBookingItems = [...cart.booking_item];

        if (index < updatedBookingItems.length) {
            updatedBookingItems[index] = date;
        } else {
            updatedBookingItems.push(date);
        }

        setCart({ ...cart, booking_item: updatedBookingItems });
        setOpen(false);
    }

    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger>
                {
                    bookingDate?.booking_date ?
                        <>
                            <div className="flex flex-col justify-start items-start">
                                <div className="mt-4 text-sm">{t("dialog_date.times2")} {index + 1}</div>
                                <div className="flex flex-row  px-4 py-2 w-full border cursor-pointer justify-between" >
                                    <div className="flex flex-row gap-4">
                                        <CalendarDays size={20} strokeWidth={2} />
                                        <div className="flex flex-col ">
                                            <div>{bookingDate?.booking_date ? format(bookingDate.booking_date, "dd MMM yyyy", { locale: currentLocale === 'th' ? th : enUS }) : ''}</div>
                                            <div className="flex text-xs mt-1 text-slate-500">{bookingDate?.booking_time_start} {bookingDate?.booking_time_finish ? ` - ${bookingDate?.booking_time_finish}` : ''}</div>
                                        </div>
                                    </div>
                                    <div className='flex justify-center items-center'>
                                        <ChevronRight size={20} strokeWidth={3} />
                                    </div>
                                </div>
                            </div>
                        </>
                        :
                        <div className='flex flex-row justify-between border p-4 cursor-pointer mt-4 shadow-md'>
                            <div className='flex flex-row gap-4 items-center'>
                                <CalendarDays size={20} strokeWidth={2} />
                                <div className="text-sm text-gray-500">{t("dialog_date.btn_add")}</div>
                            </div>
                        </div>
                }

            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>{t("dialog_date.header")}</DialogTitle>
                    <DialogDescription></DialogDescription>
                </DialogHeader>

                <form className='flex flex-col w-full'>
                    <div className="flex flex-row justify-between mb-4">
                        <div className='text-sm'>{t("dialog_date.working_date")}</div>
                        <div className='text-slate-500 text-sm'>({t("dialog_date.choose")} {cart?.times || 0} {t("dialog_date.times")})</div>
                    </div>
                    <Calendar
                        showOutsideDays={false}
                        locale={currentLocale === 'th' ? th : enUS}
                        mode="single"
                        selected={selectedDates}
                        onSelect={setSelectedDates}
                        disabled={[{ before: minDateOpen }, ...getDisabledDates()]}
                        className="flex justify-center"
                        timeZone="Asia/Bangkok"
                    />

                    <div className="flex flex-row justify-between mt-4">
                        <div className='text-sm'>{t("dialog_date.working_time")}</div>
                        <div className='text-slate-500 text-sm'>({cart?.hour_per_time || '-'} {t("dialog_date.hours")})</div>
                    </div>

                    <div className="overflow-y-auto overflow-x-hidden mt-4 mb-8">
                        <Select value={selectedTime} onValueChange={setSelectedTime}>
                            <SelectTrigger className="w-full justify-between">
                                <SelectValue placeholder={t("dialog_date.dropdown_time_placeholder")}></SelectValue>
                            </SelectTrigger>
                            <SelectContent className="overflow-y-auto">
                                <SelectItem value="08:00">08:00</SelectItem>
                                <SelectItem value="08:30">08:30</SelectItem>
                                <SelectItem value="09:00">09:00</SelectItem>
                                <SelectItem value="09:30">09:30</SelectItem>
                                <SelectItem value="10:00">10:00</SelectItem>
                                <SelectItem value="10:30">10:30</SelectItem>
                                <SelectItem value="11:00">11:00</SelectItem>
                                <SelectItem value="11:30">11:30</SelectItem>
                                <SelectItem value="12:00">12:00</SelectItem>
                                <SelectItem value="12:30">12:30</SelectItem>
                                <SelectItem value="13:00">13:00</SelectItem>
                                <SelectItem value="13:30">13:30</SelectItem>
                                <SelectItem value="14:00">14:00</SelectItem>
                                <SelectItem value="14:30">14:30</SelectItem>
                                <SelectItem value="15:00">15:00</SelectItem>
                                <SelectItem value="15:30">15:30</SelectItem>
                                <SelectItem value="16:00">16:00</SelectItem>
                                <SelectItem value="16:30">16:30</SelectItem>
                                <SelectItem value="17:00">17:00</SelectItem>
                                <SelectItem value="17:30">17:30</SelectItem>
                                <SelectItem value="18:00">18:00</SelectItem>
                            </SelectContent>
                        </Select>
                    </div>
                </form>

                <DialogFooter className="sm:justify-end gap-4">
                    <DialogClose asChild>
                        <Button type="button" variant="secondary">
                            {t("dialog_date.btn_close")}
                        </Button>
                    </DialogClose>
                    <Button type="button" variant="" onClick={onSubmit} >
                        {t("dialog_date.btn_confirm")}
                    </Button>
                </DialogFooter>
            </DialogContent>
        </Dialog>
    )
}