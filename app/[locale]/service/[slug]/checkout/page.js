"use client"

import { CalendarDays, ChevronRight,ChevronLeft, MapPin, Landmark, Banknote, X } from 'lucide-react';
import { Button } from "@/components/ui/button"
import { Separator } from "@/components/ui/separator"
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
    DialogFooter,
    DialogClose
} from "@/components/ui/dialog"
import {
    Drawer,
    DrawerClose,
    DrawerContent,
    DrawerDescription,
    DrawerFooter,
    DrawerHeader,
    DrawerTitle,
    DrawerTrigger,
    DrawerOverlay,
    DrawerPortal,
} from "@/components/ui/drawer"

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { Label } from "@/components/ui/label"
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group"
import Link from 'next/link'
import React, { useEffect, useState, useMemo, useContext } from 'react';
import Image from 'next/image'
import MemberAddress from "./member-address"
import PromotpayIcon from "../../../../../public/logo/promptpay.png"
import BookingDatesDialog from "./booking-dates-dialog"
import BookingDateDialog from "./booking-date-dialog"
import { cartStore } from '@/app/(store)/cart-store'
import { format } from "date-fns"
import { addHours } from "@/lib/dateUtils"
import { useMediaQuery } from "@/app/hooks/use-media-query"
import ConfirmBooking from "./confirm-booking"
import { useParams, useRouter } from 'next/navigation'
import { useTranslation } from 'react-i18next'
import { formatNumberWithThousandSeparator } from "@/lib/numberFormat";
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import axios from 'axios';
import { useToast } from "@/components/ui/use-toast"
import { userStore } from '@/app/(store)/auth-store';

export default function Page() {
    const router = useRouter()

    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;

    const cart = cartStore((state) => state.cart)
    const setCart = cartStore((state) => state.setCart)

    const [address, setAddress] = useState();

    useEffect(() => {
        // if (!cart || !cart?.services_id) {
        //     router.replace("/")
        // }
    }, [])

    const sortedBookingItems = useMemo(() => {
        if (!cart?.booking_item) return [];
        return [...cart.booking_item].sort((a, b) => {
            if (a.booking_date < b.booking_date) return -1;
            if (a.booking_date > b.booking_date) return 1;
            if (a.booking_time_start < b.booking_time_start) return -1;
            if (a.booking_time_start > b.booking_time_start) return 1;
            return 0;
        });
    }, [cart?.booking_item]);

    const onBack = () => {
        router.back();
    }

    return (
        <>
            <div className="flex flex-col md:flex-row bg-slate-100 min-h-screen">
                <div className="h-full w-full px-0 lg:px-[15rem] xl:px-[22rem]">
                    <div onClick={onBack} className="flex bg-white w-10 h-10 rounded-full mt-4 ml-4 items-center justify-center drop-shadow-2xl cursor-pointer">
                        <X  strokeWidth={2}/>
                    </div>
                    {/* Booking Time */}
                    <div className="flex flex-col p-4 bg-white mt-4">
                        <div className="text-lg font-medium text-blue-600 ">{t("header_bookingtime")}</div>
                        {
                            (sortedBookingItems.length > 0) ?
                                sortedBookingItems.map((item, index) => (
                                    <BookingDateDialog
                                        key={item.id || index}
                                        item={item}
                                        index={index}
                                        originalIndex={cart.booking_item.findIndex(
                                            i => i.booking_date === item.booking_date && i.booking_time_start === item.booking_time_start
                                        )}
                                    />
                                ))
                                :
                                <BookingDatesDialog />
                        }
                    </div>

                    {/* Location Info */}
                    <div className="flex flex-col p-4 bg-white mt-4">
                        <div className="text-lg font-medium text-blue-600 ">{t("location_header")}</div>
                        <MemberAddress />
                    </div>

                    {/* Services Summaries */}
                    <div className="flex flex-col p-4 bg-white mt-4">
                        <div className="text-lg font-medium text-blue-600 ">{t("service_header")}</div>

                        <div className="flex flex-col  mt-4">
                            <div className="flex flex-row justify-between gap-4">
                                <div className="font-extralight">{currentLocale === "th" ? cart?.services_name_th : cart?.services_name_en}</div>
                                <div className="font-extralight">฿{formatNumberWithThousandSeparator(cart?.price, 2)}</div>
                            </div>

                            {/* Add ons */}
                            {/* <div className="flex flex-row justify-between">
                                <div className="font-thin text-slate-500">เพิ่ม บริการฆ่าเชื้อ</div>
                                <div className="font-thin">฿300.00</div>
                            </div> */}
                        </div>

                        <Separator className="my-6" />

                        <div className="flex flex-col">
                            {/* <div className="flex flex-row justify-between">
                                <div className="font-thin">VAT</div>
                                <div className="font-thin">฿0.00</div>
                            </div>
                            <div className="flex flex-row justify-between">
                                <div className="font-thin">FEE</div>
                                <div className="font-thin">฿0.00</div>
                            </div> */}

                            {/* {
                                cart?.coupon_id &&
                                <div className="flex flex-row justify-between">
                                    <div className="font-thin">ส่วนลด</div>
                                    <div className="font-thin">฿0.00</div>
                                </div>
                            } */}

                            {
                                cart?.coupon_id &&
                                <div className="flex flex-row justify-between text-orange-500">
                                    <div className="flex flex-col ">
                                        <span className="font-extralight">{t("coupon.discount")} ({cart?.coupon_code})</span>
                                    </div>
                                    <div className="font-extralight">-฿{formatNumberWithThousandSeparator(cart?.discount_amount, 2)}</div>
                                </div>
                            }

                            {/* Total */}
                            <div className="flex flex-row justify-between">
                                <div className="font-semibold text-green-600">{t("total")}</div>
                                <div className="font-semibold text-green-600">฿{cart?.net_price ? formatNumberWithThousandSeparator(cart?.net_price, 2) : formatNumberWithThousandSeparator(cart?.price, 2)}</div>
                            </div>
                        </div>
                    </div>

                    {/* Coupon & Payment Method */}
                    <div className="flex flex-col p-4 bg-white mt-4">
                        <div className="flex flex-row justify-between">
                            <div className="text-lg font-medium text-blue-600 ">{t("coupon.header")}</div>
                            <CouponDialog />
                        </div>

                        <Separator className="my-6" />

                        <div className="text-lg font-medium text-blue-600 ">{t("payment.header")}</div>
                        <PaymentMethodDialog />
                    </div>


                    {/* Submit Button */}
                    {
                        (!cart?.booking_item || cart?.booking_item?.length === 0 || !cart?.address_name || !cart?.payment_method_id) ?
                            <div className="p-4 mt-4 mb-20">
                                <Button className="w-full h-[50px] text-lg bg-green-600 hover:bg-green-600/90" disabled={true}>
                                    {t("btn_booking")}
                                </Button>
                            </div> :
                            <ConfirmBooking />
                    }

                </div>
            </div>
        </>
    );
}




function CouponDialog() {
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;
    const cart = cartStore((state) => state.cart)
    const setCart = cartStore((state) => state.setCart)
    const [open, setOpen] = useState(false);
    const { toast } = useToast()
    const isDesktop = useMediaQuery("(min-width: 768px)")
    const userSession = userStore((state) => state?.userSession)

    useEffect(() => {

        return () => {
            form.setValue('couponCode', '')
        }
    }, [open])

    const clearCoupon = () => {
        setCart({
            coupon_id: null,
            coupon_code: null,
            discount_type: null,
            discount_value: null,
            discount_amount: null,
            net_price: null,
        })

        setOpen(false)
        toast({
            description: currentLocale === 'th' ? "ยกเลิกการใช้คูปองแล้ว" : "Coupon was removed.",
            duration: 5000,
        })
    }

    const applyCoupon = async (values) => {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/apply-coupon/${values.coupon_code}`,
            { params: { services_code: values.services_code, lineuuid: values.lineuuid } },
            {
                headers: {
                    'Content-Type': 'application/json'
                }
            });
        return response.data;
    }

    const mutation = useMutation({
        mutationFn: applyCoupon,
        retry: false,
        onSuccess: (resp) => {
            console.log('applyCoupon', resp);

            if (!resp?.success) {
                toast({
                    description: resp.message || "There was a problem with your request. please try again",
                    duration: 5000,
                })

                return
            }

            setCart({
                coupon_id: resp?.data?.coupon_id,
                coupon_code: resp?.data?.coupon_code,
                discount_type: resp?.data?.discount_type,
                discount_value: resp?.data?.discount_value,
                discount_amount: resp?.data?.discount_amount,
                net_price: resp?.data?.net_price,
            })
            toast({
                description: currentLocale === 'th' ? "คุณได้รับคูปองส่วนลดแล้ว" : "Coupon was applied.",
                duration: 5000,
            })
            setOpen(false)
        },
        onError: (error) => {
            toast({
                description: "There was a problem with your request. please try again",
                duration: 5000,
            })
        },
    })

    const formSchema = z.object({
        couponCode: z.string().min(3, {
            message: currentLocale === "th" ? "กรุณาระบุคูปองส่วนลด" : "Please specify a code",
        })
    })


    const form = useForm({
        resolver: zodResolver(formSchema),
        defaultValues: {
            couponCode: "",
        },
    })

    function onSubmit(values) {
        const req = { coupon_code: values.couponCode, services_code: cart.services_code, lineuuid: userSession.line_uuid }
        console.log("onSubmit", req)
        mutation.mutate(req);
    }
    const openHandler = (val) => {
        console.log('openHandler', val);
        setOpen(val);
    };

    if (cart?.coupon_id) {
        if (isDesktop) {
            return (
                <>
                    <Dialog open={open} onOpenChange={openHandler}>
                        <DialogTrigger>
                            <div className="flex flex-row items-center gap-2 cursor-pointer">
                                <div className="text-slate-300">{cart?.coupon_code}</div>
                                <X size={20} strokeWidth={3} className="text-slate-500" />
                            </div>
                        </DialogTrigger>
                        <DialogContent>
                            <DialogHeader>
                                <DialogTitle>{t("coupon.remove.header")}</DialogTitle>
                                <DialogDescription>{t("coupon.remove.description")}</DialogDescription>
                            </DialogHeader>
                            <DialogFooter className="sm:justify-end gap-4 mt-4">
                                <DialogClose asChild>
                                    <Button type="button" variant="secondary">
                                        {t("coupon.remove.cancel")}
                                    </Button>
                                </DialogClose>

                                <Button type="button" variant="" onClick={clearCoupon}>
                                    {t("coupon.remove.accept")}
                                </Button>
                            </DialogFooter>
                        </DialogContent>
                    </Dialog>
                </>
            )
        }
        return (
            <>
                <Drawer open={open} onOpenChange={setOpen}>
                    <DrawerTrigger>
                        <div className="flex flex-row items-center gap-2 cursor-pointer">
                            <div className="text-slate-300">{cart?.coupon_code}</div>
                            <X size={20} strokeWidth={3} className="text-slate-500" />
                        </div>
                    </DrawerTrigger>
                    <DrawerContent>
                        <DrawerHeader className="text-left">
                            <DrawerTitle>{t("coupon.remove.header")}</DrawerTitle>
                            <DrawerDescription>{t("coupon.remove.description")}</DrawerDescription>
                        </DrawerHeader>
                        <DrawerFooter>
                            <Button type="button" variant="" onClick={clearCoupon}>
                                {t("coupon.remove.accept")}
                            </Button>
                            <DialogClose asChild>
                                <Button type="button" variant="secondary">
                                    {t("coupon.remove.cancel")}
                                </Button>
                            </DialogClose>
                        </DrawerFooter>
                    </DrawerContent>
                </Drawer>

            </>
        )
    }


    if (isDesktop) {
        return (
            <>
                <Dialog open={open} onOpenChange={openHandler}>
                    <DialogTrigger>
                        <div className="flex flex-row items-center gap-2 cursor-pointer">
                            <div className="text-slate-300">{t("coupon.use")}</div>
                            <ChevronRight size={20} strokeWidth={3} className="text-slate-500" />
                        </div>
                    </DialogTrigger>
                    <DialogContent>
                        <DialogHeader>
                            <DialogTitle>{t("coupon.header")}</DialogTitle>
                            <DialogDescription></DialogDescription>
                            <Form {...form}>
                                <form onSubmit={form.handleSubmit(onSubmit)} className='h-full'>
                                    <div className='flex flex-col mt-4'>
                                        <FormField
                                            control={form.control}
                                            name="couponCode"
                                            render={({ field }) => (
                                                <FormItem>
                                                    <FormLabel></FormLabel>
                                                    <FormControl>
                                                        <Input placeholder={t("coupon.textbox")} {...field} />
                                                    </FormControl>
                                                    <FormMessage />
                                                </FormItem>
                                            )}
                                        />
                                    </div>

                                    <div className="flex sm:justify-end gap-4 w-full mt-6">
                                        <Button disabled={mutation.isPending} type="submit" className="w-full sm:w-auto">{t("coupon.btn_confirm")}</Button>
                                    </div>
                                </form>
                            </Form>
                        </DialogHeader>
                    </DialogContent>
                </Dialog>
            </>
        );
    } else {

        return (
            <>
                <Drawer open={open} onOpenChange={openHandler}>
                    <DrawerTrigger>
                        <div className="flex flex-row items-center gap-2 cursor-pointer">
                            <div className="text-slate-300">{t("coupon.use")}</div>
                            <ChevronRight size={20} strokeWidth={3} className="text-slate-500" />
                        </div>
                    </DrawerTrigger>
                    <DrawerContent>
                        <DrawerHeader className="text-left">
                            <DrawerTitle>{t("coupon.header")}</DrawerTitle>
                            <DrawerDescription></DrawerDescription>
                        </DrawerHeader>
                        <div className="px-6 pb-12">
                            <Form {...form}>
                                <form onSubmit={form.handleSubmit(onSubmit)} className='h-full'>
                                    <div className='flex flex-col mt-4'>
                                        <FormField
                                            control={form.control}
                                            name="couponCode"
                                            render={({ field }) => (
                                                <FormItem>
                                                    <FormLabel></FormLabel>
                                                    <FormControl>
                                                        <Input placeholder={t("coupon.textbox")} {...field} />
                                                    </FormControl>
                                                    <FormMessage />
                                                </FormItem>
                                            )}
                                        />
                                    </div>

                                    <div className="flex sm:justify-end gap-4 w-full mt-6">
                                        <Button disabled={mutation.isPending} type="submit" className="w-full sm:w-auto">{t("coupon.btn_confirm")}</Button>
                                    </div>
                                </form>
                            </Form>
                        </div>

                    </DrawerContent>
                </Drawer>
            </>
        )
    }


}

function PaymentMethodDialog() {

    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;
    const cart = cartStore((state) => state.cart)
    const setCart = cartStore((state) => state.setCart)

    const [open, setOpen] = useState(false)

    const formSchema = z.object({
        payment_method_id: z.string({
            required_error: currentLocale === "th" ? "โปรดระบุช่องทางการชำระเงิน" : "Please choose a payment method.",
            invalid_type_error: currentLocale === "th" ? "โปรดระบุช่องทางการชำระเงิน" : "Please choose a payment method.",
        }).min(1, {
            message: currentLocale === "th" ? "โปรดระบุช่องทางการชำระเงิน" : "Please choose a payment method."
        }),
    })

    const form = useForm({
        resolver: zodResolver(formSchema),
        defaultValues: {
            payment_method_id: cart?.payment_method_id || null,
        },
    })

    const fetchPaymentMethods = async () => {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/payment-methods/`);
        console.log('fetchPaymentMethods', response);
        return response.data;
    }

    const { isPending, isError, data, error } = useQuery({
        queryKey: ['paymentMethod'],
        queryFn: fetchPaymentMethods,
        refetchOnWindowFocus: false,
    })

    if (isPending) {
        return (<></>)
    }


    const onSubmit = (values) => {
        // Do something with the form values.
        // ✅ This will be type-safe and validated.
        console.log("onSubmit", values)
        if (!values || !values?.payment_method_id) return;

        setCart({ payment_method_id: values.payment_method_id })
        setOpen(false)
    }

    const findPaymentDescription = (id) => {
        if (!id || !data?.data) {
            return currentLocale === "th" ? "ระบุวิธีชำระเงิน" : "Choose a payment method"
        }

        return currentLocale === "th" ? data?.data?.find(f => f.id === id)?.method_name_th : data?.data?.find(f => f.id === id)?.method_name_en
    }

    return (
        <>
            <Dialog open={open} onOpenChange={setOpen}>
                <DialogTrigger>
                    <div className="flex flex-row justify-between mt-2 ">
                        <div className="font-extralight">{findPaymentDescription(cart?.payment_method_id)}</div>
                        <ChevronRight size={20} strokeWidth={3} className="text-slate-500" />
                    </div>
                </DialogTrigger>
                <DialogContent>
                    <DialogHeader>
                        <DialogTitle>{t("payment.header")}</DialogTitle>
                        <DialogDescription></DialogDescription>
                        <Form {...form}>
                            <form onSubmit={form.handleSubmit(onSubmit)} className='h-full'>
                                <div className='flex flex-col mt-4'>
                                    <FormField
                                        control={form.control}
                                        name="payment_method_id"
                                        render={({ field }) => (
                                            <FormItem className="space-y-3">
                                                <FormLabel></FormLabel>
                                                <FormControl>
                                                    <RadioGroup
                                                        onValueChange={field.onChange}
                                                        defaultValue={cart?.payment_method_id}
                                                        className="flex flex-col space-y-1"
                                                    >
                                                        {data?.data?.map((method, index) => (
                                                            <FormItem key={index} className="flex items-center space-x-3 space-y-0">
                                                                <FormControl>
                                                                    <RadioGroupItem value={method.id} />
                                                                </FormControl>
                                                                <FormLabel className="font-normal flex flex-row items-end gap-2">
                                                                    {/* <Image src={PromotpayIcon} width={18} height={18} alt='Promptpay icon' /> */}
                                                                    <span>{currentLocale === "th" ? method?.method_name_th : method?.method_name_en}</span>
                                                                </FormLabel>
                                                            </FormItem>
                                                        ))}
                                                    </RadioGroup>
                                                </FormControl>
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                </div>

                                <div className="flex sm:justify-end gap-4 w-full mt-6">
                                    <Button type="submit" className="w-full sm:w-auto">{t("payment.btn_confirm")}</Button>
                                </div>
                            </form>
                        </Form>

                    </DialogHeader>
                </DialogContent>
            </Dialog>
        </>
    );
}