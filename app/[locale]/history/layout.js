import TranslationsProvider from '@/app/providers/TranslationProvider'
import initTranslations from '@/app/i18n'

export default async function HistoryLayout({ children, params: { locale } }) {
    const i18nNamespace = ['service'];
    const { resources } = await initTranslations(locale, i18nNamespace);

    return (
        <>
            <TranslationsProvider
                namespaces={i18nNamespace}
                locale={locale}
                resources={resources}>
                <div className="flex flex-col w-full">
                    <div className="mb-30">{children}</div>
                </div>
            </TranslationsProvider>
        </>
    );
}