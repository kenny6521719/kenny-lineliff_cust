"use client"

import React, { useState, useEffect, useRef, useCallback } from 'react'
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs"
import { Card, CardHeader, CardTitle, CardDescription, CardContent } from "@/components/ui/card"
import { Badge } from "@/components/ui/badge"
import { MapPin, ChevronRight, SquareUser } from 'lucide-react';
import { useTranslation } from 'react-i18next'
import { useInView } from 'react-intersection-observer';
import { userStore } from '@/app/(store)/auth-store';
import { useQueryClient, useInfiniteQuery } from '@tanstack/react-query';
import axios from 'axios'
import { format } from "date-fns"
import { formatTime } from '@/lib/dateUtils'
import { useParams, useRouter } from 'next/navigation'
import { formatNumberWithThousandSeparator } from "@/lib/numberFormat";
import { Skeleton } from "@/components/ui/skeleton";


const ItemsSkeleton = () => {

    return (
        <>
            <div className="grid gap-4">
                {
                    [...Array(3)].map((_, index) => (
                        <div key={index} className="flex flex-row w-full justify-between border rounded-lg p-4 cursor-pointer hover:border-black/20">
                            <div className="flex flex-col">
                                <Skeleton className="h-3 w-24 mb-1" /> {/* booking_no */}
                                <Skeleton className="h-4 w-48 mb-1" /> {/* date and time */}
                                <Skeleton className="h-3.5 w-40 mb-2" /> {/* service name */}

                                <div className="flex flex-row gap-2 mt-1 items-center">
                                    <Skeleton className="h-4 w-4" /> {/* MapPin icon */}
                                    <Skeleton className="h-3.5 w-32" /> {/* address name */}
                                </div>

                                <div className="flex flex-row gap-2 mt-1 items-center">
                                    <Skeleton className="h-4 w-4" /> {/* SquareUser icon */}
                                    <Skeleton className="h-3.5 w-40" /> {/* staff name */}
                                </div>

                            </div>

                            <div className="flex flex-col justify-between items-end">
                                <Skeleton className="h-4 w-16" /> {/* price */}
                                <Skeleton className="h-4 w-4 mb-2" /> {/* ChevronRight icon */}
                            </div>
                        </div>
                    ))
                }
            </div>


        </>
    )
}

const fetchBookings = async ({ pageParam = 1, queryKey }) => {
    const [_, memberId, status] = queryKey;
    const response = await axios.post(`${process.env.NEXT_PUBLIC_BASE_API}/api/liff/booking-history/${memberId}`, {
        criteria: {
            booking_status: status,
        },
        gridCriteria: {
            page: pageParam,
            pageSize: 20,
            sortby: 'CreateDate',
            sortdir: 'desc',
        },
    });
    return response.data;
};

export default function Page() {
    const router = useRouter()
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;
    const userSession = userStore((state) => state?.userSession)

    const [activeTab, setActiveTab] = useState('IN-PROGRESS');
    const { ref, inView } = useInView();
    const queryClient = useQueryClient();
    const scrollRef = useRef(null);
    const isFetchingRef = useRef(false);

    const {
        data,
        error,
        fetchNextPage,
        hasNextPage,
        isFetchingNextPage,
        status,
        refetch,
    } = useInfiniteQuery({
        queryKey: ['history', userSession?.id, activeTab],
        queryFn: fetchBookings,
        initialPageParam: 1,
        getNextPageParam: (lastPage) => {
            if (lastPage?.pagination?.page < lastPage?.pagination?.totalPage) {
                return lastPage?.pagination?.page + 1;
            }
            return undefined;
        },
        enabled: !!userSession?.id
    });

    const handleScroll = useCallback(() => {
        const element = scrollRef.current;
        if (!element || isFetchingRef.current) return;

        const { scrollTop, clientHeight, scrollHeight } = element;
        if (scrollHeight - scrollTop <= clientHeight + 1) {
            if (hasNextPage && !isFetchingNextPage) {
                isFetchingRef.current = true;
                fetchNextPage().finally(() => {
                    isFetchingRef.current = false;
                });
            }
        }
    }, [fetchNextPage, hasNextPage, isFetchingNextPage]);

    useEffect(() => {
        const element = scrollRef.current;
        if (element) {
            element.addEventListener('scroll', handleScroll);
            return () => element.removeEventListener('scroll', handleScroll);
        }
    }, [handleScroll]);

    useEffect(() => {
        if (inView && hasNextPage) {
            fetchNextPage()
        }
    }, [inView, fetchNextPage, hasNextPage])

    useEffect(() => {
        if (data) {
            console.log('Data updated:', data);
        }
    }, [data]);


    const handleTab = (tab) => {
        console.log('handleTab', tab)
        setActiveTab(tab)
        queryClient.resetQueries(['history', userSession.id, tab])
    }

    const handleBooking = (e, booking) => {
        e.preventDefault()

        router.push(`/history/${booking.id}`)

    }

    if (status === "error") return (<div>Error: {error.message}</div>)

    const renderBookingList = () =>
    (
        <div className="grid gap-4">
            <div className=""></div>

            {/* loop this */}
            {data?.pages.map((page, pageIndex) => (
                <React.Fragment key={pageIndex}>
                    {page.items.map((booking) => (
                        <div key={booking.id} onClick={(e) => handleBooking(e, booking)} className="flex flex-row w-full justify-between border rounded-lg p-4 cursor-pointer hover:border-black/20" >
                            <div className="flex flex-col">
                                <div className="text-xs font-semibold text-green-500">{booking?.booking_no}</div>
                                <div className="text-sm font-light text-black/80">{format(booking?.booking_date, "dd MMM yyyy")} {formatTime(booking?.booking_time_start)} - {formatTime(booking?.booking_time_finish)}</div>
                                <div className="text-sm font-thin text-black/50 flex gap-2">
                                    {currentLocale === "th" ? booking?.services_name_th : booking?.services_name_en}
                                    {booking.category_code === "PACKAGE" && <span>{`(${booking?.round_at}/${booking?.round_of})`}</span>}
                                </div>
                                <div className="flex flex-row gap-2 mt-1">
                                    <MapPin size={18} strokeWidth={2} className="text-red-500" />
                                    <div className="text-sm font-medium">{booking?.address?.address_name}</div>
                                </div>
                                {
                                    (booking?.staff?.fname) ?
                                        <div className="flex flex-row gap-2 mt-1">
                                            <SquareUser size={18} strokeWidth={2} className="text-green-500" />
                                            <div className="text-sm font-medium">{booking?.staff?.fname} {booking?.staff?.lname} {booking?.staff?.nickname ? `(${booking?.staff?.nickname})` : ""}</div>
                                        </div> :
                                        <></>
                                }
                                <div className="flex flex-row gap-2 flex-wrap mt-2">
                                    <Badge className="min-w-fit">{currentLocale === "th" ? booking.booking_status.description_th : booking.booking_status.description_en}</Badge>

                                    {booking?.payment_status?.code
                                        ? <Badge variant="outline" className="min-w-fit">{currentLocale === "th" ? booking?.payment_status?.description_th : booking?.payment_status?.description_en}</Badge>
                                        : <></>
                                    }

                                    {booking?.staff_status?.code
                                        && <Badge variant="secondary" className="min-w-fit">{currentLocale === "th" ? booking?.staff_status?.description_th : booking?.staff_status?.description_en}</Badge>
                                    }

                                </div>
                            </div>
                            <div className="flex flex-col justify-between items-end">
                                <div className="font-medium text-orange-500">฿{formatNumberWithThousandSeparator(booking.net_price, 2)}</div>
                                <ChevronRight size={18} strokeWidth={4} className="mb-2" />
                            </div>
                        </div>
                    ))}
                </React.Fragment>
            ))}
            <div ref={ref}>
                {isFetchingNextPage
                    ? 'Loading more...'
                    : hasNextPage
                        ? 'Load more'
                        : ''}
            </div>
        </div>
    )


    return (
        <>
            <div className="flex flex-col min-h-screen bg-slate-100 items-center overflow-hidden">
                <Card className="w-full max-w-4xl sm:my-10 flex flex-col min-h-[calc(100vh-5rem)]">
                    <CardHeader>
                        <CardTitle>ประวัติการใช้บริการ</CardTitle>
                        <CardDescription></CardDescription>
                    </CardHeader>
                    <CardContent className="flex-grow overflow-hidden">
                        <Tabs className="w-full" value={activeTab} onValueChange={handleTab}>
                            <TabsList className="grid grid-cols-3 border-b ">
                                <TabsTrigger value="IN-PROGRESS">In Progress</TabsTrigger>
                                <TabsTrigger value="COMPLETED">Completed</TabsTrigger>
                                <TabsTrigger value="CANCELLED">Cancelled / Failed</TabsTrigger>
                            </TabsList>
                            <TabsContent value={activeTab} className="flex-grow overflow-y-auto" ref={scrollRef}>
                                {status === "pending"
                                    ? <ItemsSkeleton />
                                    : <>
                                        {renderBookingList()}
                                        {isFetchingNextPage && <div className="text-center py-4">Loading more...</div>}
                                    </>
                                }
                            </TabsContent>
                        </Tabs>
                    </CardContent>
                </Card>
            </div>
        </>
    )
}