"use client"

import React, { useEffect, useState, useContext } from 'react'
import { useParams, useRouter } from 'next/navigation'
import axios from 'axios'
import { CalendarDays, ChevronRight, ChevronLeft, MapPin, X } from 'lucide-react';
import { format } from "date-fns"
import { useTranslation } from 'react-i18next'
import { Badge } from "@/components/ui/badge"
import { Avatar, AvatarFallback, AvatarImage } from "@/components/ui/avatar"
import StarRating from '@/app/(components)/star-rating'
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
    DialogFooter,
    DialogClose
} from "@/components/ui/dialog"
import { Button } from "@/components/ui/button"
import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"
import { Calendar } from "@/components/ui/calendar"
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import { addHours, formatTime } from "@/lib/dateUtils"
import { Separator } from "@/components/ui/separator"
import { formatNumberWithThousandSeparator } from "@/lib/numberFormat";
import { Skeleton } from "@/components/ui/skeleton";
import { LiffContext } from '@/app/providers/LiffProvider'
import { th, enUS } from "date-fns/locale";

const updateBookingDate = async (values) => {
    const response = await axios.post(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/update-booking-date/`, values, {
        headers: {
            'Content-Type': 'application/json'
        }
    });
    return response.data;
}

function BookingDateDialog({ booking, booking_item }) {
    const [open, setOpen] = useState(false);
    const [selectedDate, setSelectedDate] = useState([])
    const [selectedTime, setSelectedTime] = useState('')
    const [valid, setIsValid] = useState(false)
    const queryClient = useQueryClient();

    useEffect(() => {
        // console.log("booking", booking)
        // console.log("booking_item", booking_item)

        return () => {
            console.log("cleaned up");
            setSelectedTime(null)
            setSelectedDate(null)
        }
    }, [])

    useEffect(() => {
        if (selectedTime && selectedDate) {
            setIsValid(true);
        } else {
            setIsValid(false);
        }
    }, [selectedTime, selectedDate])


    const mutation = useMutation({
        mutationFn: updateBookingDate,
        retry: false,
        onSuccess: (res) => {
            console.log('mutation success', res);
            if (res?.success === true) {
                queryClient.resetQueries(['history-detail'])
            }
        },
        onError: (err) => {
            console.log('mutation error', err);
        },
        onSettled: () => {
            setOpen(false)
        }
    })
    const onSubmit = (e) => {
        e.preventDefault();

        if (!selectedTime || !selectedDate) return

        console.log("submit selectedDates", selectedDate)
        console.log("submit selectedTime", selectedTime)

        const req = {
            id: booking_item.id,
            booking_date: format(selectedDate, 'yyyy-MM-dd'),
            booking_time_start: selectedTime,
            booking_time_finish: addHours(selectedTime, booking?.hour_per_time),
        }

        console.log("req", req);
        mutation.mutate(req)
    }

    return (
        <>
            <Dialog open={open} onOpenChange={setOpen}>
                <DialogTrigger asChild>
                    <div className='flex flex-row justify-between border p-4 cursor-pointer mt-4 shadow-md'>
                        <div className='flex flex-row gap-4'>
                            <CalendarDays size={20} strokeWidth={2} />
                            <div>Pick a dates</div>
                        </div>
                        <div className='flex justify-center items-center'>
                            <ChevronRight size={20} strokeWidth={2} />
                        </div>
                    </div>
                </DialogTrigger>
                <DialogContent>
                    <DialogHeader>
                        <DialogTitle>Booking Time</DialogTitle>
                        <DialogDescription></DialogDescription>
                    </DialogHeader>

                    <form className='flex flex-col w-full'>
                        <div className="flex flex-row justify-between mb-4">
                            <div className='text-sm'>กำหนดวันทำงาน</div>
                            <div className='text-slate-500 text-sm'>(เลือกได้ 1 วัน)</div>
                        </div>
                        <Calendar
                            mode="single"
                            selected={selectedDate}
                            onSelect={setSelectedDate}
                            disabled={{ before: new Date() }}
                            min={0}
                            max={1}
                            className="flex justify-center"
                        />

                        <div className="flex flex-row justify-between mt-4">
                            <div className='text-sm'>กำหนดเวลาเริ่มงาน</div>
                            <div className='text-slate-500 text-sm'>(ทำงาน {booking?.hour_per_time} ชั่วโมง)</div>
                        </div>

                        <div className="overflow-y-auto overflow-x-hidden mt-4 mb-8">
                            <Select value={selectedTime} onValueChange={setSelectedTime}>
                                <SelectTrigger className="w-full justify-between">
                                    <SelectValue placeholder="Select time"></SelectValue>
                                </SelectTrigger>
                                <SelectContent className="overflow-y-auto">
                                    <SelectItem value="08:00">08:00</SelectItem>
                                    <SelectItem value="08:30">08:30</SelectItem>
                                    <SelectItem value="09:00">09:00</SelectItem>
                                    <SelectItem value="09:30">09:30</SelectItem>
                                    <SelectItem value="10:00">10:00</SelectItem>
                                    <SelectItem value="10:30">10:30</SelectItem>
                                    <SelectItem value="11:00">11:00</SelectItem>
                                    <SelectItem value="11:30">11:30</SelectItem>
                                    <SelectItem value="12:00">12:00</SelectItem>
                                    <SelectItem value="12:30">12:30</SelectItem>
                                    <SelectItem value="13:00">13:00</SelectItem>
                                    <SelectItem value="13:30">13:30</SelectItem>
                                    <SelectItem value="14:00">14:00</SelectItem>
                                    <SelectItem value="14:30">14:30</SelectItem>
                                    <SelectItem value="15:00">15:00</SelectItem>
                                    <SelectItem value="15:30">15:30</SelectItem>
                                    <SelectItem value="16:00">16:00</SelectItem>
                                    <SelectItem value="16:30">16:30</SelectItem>
                                    <SelectItem value="17:00">17:00</SelectItem>
                                    <SelectItem value="17:30">17:30</SelectItem>
                                    <SelectItem value="18:00">18:00</SelectItem>
                                </SelectContent>
                            </Select>
                        </div>

                    </form>

                    <DialogFooter className="sm:justify-end gap-4">
                        <DialogClose asChild>
                            <Button type="button" variant="secondary">
                                Close
                            </Button>
                        </DialogClose>
                        <Button type="button" variant="" onClick={onSubmit} disabled={!valid || mutation.isPending}>
                            ยืนยัน
                        </Button>
                    </DialogFooter>


                </DialogContent>
            </Dialog>
        </>
    )
}
const BookingSkeleton = () => {
    return (
        <div className="flex flex-col bg-slate-100 min-h-screen">
            <div className="px-0 lg:px-[15rem] xl:px-[22rem] mt-4 ml-4 flex flex-row justify-between">
                <Skeleton className="h-3.5 w-48 bg-white" />
                <Skeleton className="h-3.5 w-24 mr-4 bg-white" />
            </div>

            <div className="h-screen w-full px-0 lg:px-[15rem] xl:px-[22rem]">
                {/* Booking Time */}
                <div className="flex flex-col p-4 bg-white mt-4">
                    <Skeleton className="h-3.5 w-36 mb-4" />
                    <div className="flex flex-row mt-4 px-4 py-2 w-full border justify-between bg-slate-50">
                        <div className="flex flex-row gap-4">
                            <Skeleton className="h-3.5 w-3.5" />
                            <div className="flex flex-col">
                                <Skeleton className="h-2.5 w-32" />
                                <Skeleton className="h-2 w-24 mt-1" />
                            </div>
                        </div>
                        <Skeleton className="h-3.5 w-20" />
                    </div>
                </div>

                {/* Current Staff */}
                <div className="flex flex-col p-4 bg-white mt-4">
                    <Skeleton className="h-3.5 w-24 mb-4" />
                    <div className="flex flex-row mt-4 gap-4">
                        <Skeleton className="h-[36px] w-[36px] rounded-full" />
                        <div className="flex flex-col">
                            <Skeleton className="h-2.5 w-48" />
                            <Skeleton className="h-2 w-24 mt-1" />
                        </div>
                    </div>
                </div>

                {/* Location Info */}
                <div className="flex flex-col p-4 bg-white mt-4">
                    <Skeleton className="h-3.5 w-36 mb-4" />
                    <div className="flex flex-row mt-4 px-4 py-2 w-full border justify-between">
                        <div className="flex flex-row gap-4">
                            <Skeleton className="h-3.5 w-3.5" />
                            <div className="flex flex-col">
                                <Skeleton className="h-2.5 w-32" />
                                <Skeleton className="h-2 w-full mt-2" />
                                <Skeleton className="h-2 w-48 mt-2" />
                            </div>
                        </div>
                    </div>
                </div>

                {/* Services Summaries */}
                <div className="flex flex-col p-4 bg-white mt-4">
                    <Skeleton className="h-3.5 w-28 mb-4" />
                    <div className="flex flex-row justify-between mt-4">
                        <Skeleton className="h-2.5 w-32" />
                        <Skeleton className="h-2.5 w-16" />
                    </div>
                    <Separator className="my-6" />
                    <div className="flex flex-row justify-between">
                        <Skeleton className="h-2.5 w-16" />
                        <Skeleton className="h-2.5 w-16" />
                    </div>
                </div>

                {/* Coupon & Payment Method */}
                <div className="flex flex-col p-4 bg-white mt-4">
                    <div className="flex flex-row justify-between">
                        <Skeleton className="h-3.5 w-24" />
                        <Skeleton className="h-2.5 w-24" />
                    </div>
                    <Separator className="my-6" />
                    <Skeleton className="h-3.5 w-36 mb-2" />
                    <Skeleton className="h-2.5 w-32 mt-2" />
                </div>

                <div className="p-4 mt-4 mb-20">
                    <Skeleton className="w-full h-[36px]" />
                </div>
            </div>
        </div>
    );
};

const fetchBooking = async (id) => {
    console.log('fetchBooking', id)
    const response = await axios.get(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/booking/${id}`);
    return response.data;
}

export default function Page() {
    const router = useRouter()
    const params = useParams()
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;
    const { liff } = useContext(LiffContext);

    const { isPending, isError, data, error } = useQuery({
        queryKey: ['history-detail'],
        queryFn: () => fetchBooking(params?.booking_id),
        // retry: 3,
        enabled: !!params?.booking_id
    })

    // useEffect(() => {
    //     console.log('history id', params);
    // }, [params?.booking_id])


    if (isPending) {
        return <BookingSkeleton />
    }

    if (isError) {
        return <span>Error: {error.message}</span>
    }

    if (data) {
        console.log('data', data)
    }


    const handleContactAdmin = async (e) => {
        if (liff.isInClient() === true) {

            if (liff.getContext().type !== 'none') {

                await liff.sendMessages([
                    {
                        type: "text",
                        text: "ติดต่อเจ้าหน้าที่"
                    }
                ])
            }

            liff.closeWindow()

        }
    }

    const handlePay = async (e) => {
        if (!data?.data?.id) return
        router.push(`/payment/${data?.data?.id}`)
    }

    const handleClose = async (e) => {
        router.push('/')
    }

    const onBack = () => {
        router.back();
    }

    return (
        <>
            <div className="flex flex-col bg-slate-100 min-h-screen h-full overflow-y-auto">

                <div className="px-0 lg:px-[15rem] xl:px-[22rem]  mt-4 ml-4 flex flex-row justify-between">

                    <div className="flex flex-row items-center gap-4">
                        <div onClick={onBack} className=" cursor-pointer">
                            <ChevronLeft strokeWidth={2} />
                        </div>
                        <div className="flex flex-col sm:flex-row font-medium sm:gap-2">Booking no: <span className="text-green-600 font-medium">{data.data?.booking_no}</span></div>
                    </div>
                    <div className="text-sm mr-4 text-blue-500">{currentLocale === "th" ? data.data?.booking_status?.description_th : data.data?.booking_status?.description_en}</div>
                </div>
                <div className="flex-grow w-full px-0 lg:px-[15rem] xl:px-[22rem]">

                    {/* Booking Time */}
                    <div className="flex flex-col p-4 bg-white mt-4">
                        <div className="text-lg font-medium text-blue-600 ">{t("header_bookingtime")}</div>
                        {
                            data?.data?.booking_items.map((item, itemIndex) => (item?.booking_date) ? (
                                <div key={item.id} className="flex flex-row mt-4 px-4 py-2 w-full border justify-between bg-slate-50" >
                                    <div className="flex flex-row gap-4">
                                        <CalendarDays size={20} strokeWidth={2} />
                                        <div className="flex flex-col">
                                            <div>{item?.booking_date ? format(item.booking_date, "dd MMM yyyy", { locale: currentLocale === 'th' ? th : enUS }) : ''}</div>
                                            <div className="text-xs mt-1 text-slate-500">{formatTime(item?.booking_time_start)} {item?.booking_time_finish ? ` - ${formatTime(item?.booking_time_finish)}` : ''}</div>
                                        </div>
                                    </div>
                                    {
                                        item?.booking_staff_status &&
                                        (<div className='flex justify-center items-center'>
                                            <Badge className="min-w-fit">{currentLocale === "th" ? item?.booking_staff_status?.description_th : item?.booking_staff_status?.description_en}</Badge>
                                        </div>)
                                    }

                                </div>
                            ) : (
                                <BookingDateDialog key={item.id} booking={data?.data} booking_item={item} />
                            ))
                        }
                    </div>

                    {/* Current Staff */}
                    {data.data?.staff?.StaffId && (
                        <div className="flex flex-col p-4 bg-white mt-4">
                            <div className="text-lg font-semibold text-blue-600 ">Staff</div>
                            <div className="flex flex-row mt-4 gap-4">
                                <Avatar className="h-[45px] w-[45px]" >
                                    <AvatarImage src="https://github.com/shadcn.png" />
                                    <AvatarFallback>CN</AvatarFallback>
                                </Avatar>

                                <div className="flex flex-col">
                                    <div className="font-semibold">{data?.data?.staff?.fname} {data?.data?.staff?.fname} ({data?.data?.staff?.nickname})</div>
                                    {data?.data?.staff?.rating && <StarRating rating={data?.data?.staff?.rating} />}
                                </div>

                            </div>
                        </div>
                    )}


                    {/* Location Info */}
                    <div className="flex flex-col p-4 bg-white mt-4 max-w-full overflow-hidden">
                        <div className="text-lg font-medium text-blue-600">{t("location_header")}</div>
                        <div className="flex flex-row mt-4 px-4 py-2 w-full border justify-between overflow-hidden">
                            <div className="flex flex-row gap-4 flex-grow min-w-0">
                                <MapPin size={20} strokeWidth={2} />
                                <div className="flex flex-col flex-grow min-w-0">
                                    <div className="font-normal">{data.data?.booking_address?.address_name}</div>
                                    <div className="text-xs font-light mt-2 text-slate-500 overflow-hidden">
                                        <p className="truncate">{data.data?.booking_address?.address_full}</p>
                                    </div>
                                    <div className="text-xs mt-2 text-slate-500 font-light">
                                        {data.data?.booking_address?.contact_name} {data.data?.booking_address?.contact_phone}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* Services Summaries */}
                    <div className="flex flex-col p-4 bg-white mt-4">
                        <div className="text-lg font-medium text-blue-600 ">{t("service_header")}</div>

                        <div className="flex flex-col  mt-4">
                            <div className="flex flex-row justify-between">
                                <div className="font-extralight">{currentLocale === "th" ? data.data?.services_name_th : data.data?.services_name_en}</div>
                                <div className="font-extralight">฿{formatNumberWithThousandSeparator(data.data?.price, 2)}</div>
                            </div>
                        </div>

                        <Separator className="my-6" />

                        {
                            data.data?.coupon_id &&
                            <div className="flex flex-row justify-between text-orange-500">
                                <div className="flex flex-col ">
                                    <span className="font-extralight">{t("coupon.discount")} ({data.data?.coupon_code})</span>
                                </div>
                                <div className="font-extralight">-฿{formatNumberWithThousandSeparator(data.data?.discount_amount, 2)}</div>
                            </div>
                        }

                        <div className="flex flex-col">

                            {/* Total */}
                            <div className="flex flex-row justify-between">
                                <div className="font-medium text-green-600">{t("total")}</div>
                                <div className="font-medium text-green-600">฿{data.data?.net_price ? formatNumberWithThousandSeparator(data.data?.net_price, 2) : formatNumberWithThousandSeparator(data.data?.price, 2)}</div>
                            </div>
                        </div>
                    </div>

                    {/* Coupon & Payment Method */}
                    <div className="flex flex-col p-4 bg-white mt-4">
                        <div className="flex flex-row justify-between">
                            <div className="text-lg font-semibold text-blue-600 ">{t("coupon.header")}</div>
                            <div className="flex flex-row items-center gap-2 cursor-default">
                                <div className="text-slate-500 text-md">{data.data?.coupon_code}</div>
                            </div>
                        </div>
                        <Separator className="my-6" />
                        <div className="flex flex-row justify-between">
                            <div className="text-lg font-semibold text-blue-600 ">{t("payment.header")}</div>
                            <div className="flex flex-col">
                                <div className="text-md text-slate-500">{currentLocale === "th" ? data.data?.payment_method?.method_name_th : data.data?.payment_method?.method_name_en}</div>
                                <div className="text-sm text-slate-500">{currentLocale === "th" ? data.data?.payment_status?.description_th : data.data?.payment_status?.description_en}</div>
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-col gap-4 p-4 mt-4 mb-20">
                        {(data.data?.payment_status?.code === 'AWAITING-PAYMENT' || data.data?.payment_status?.code === 'AWAITING-PAYMENT-PROOF') && <Button onClick={handlePay} className="w-full h-[50px] text-lg bg-orange-400 hover:bg-orange-400/90">{t("pay")}</Button>}
                        <Button className="w-full h-[50px] text-lg bg-green-600 hover:bg-green-600/90" onClick={handleContactAdmin}>
                            {t("contactus")}
                        </Button>
                        <Button variant="outline" className="w-full h-[50px] text-lg" onClick={handleClose}>
                            {t("home")}
                        </Button>
                    </div>
                </div>
            </div>
        </>
    )
}