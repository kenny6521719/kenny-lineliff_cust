"use client"

import { Button } from "@/components/ui/button"
import { Separator } from "@/components/ui/separator"
import Link from 'next/link'
import { useEffect } from "react"
import { useParams, useRouter } from 'next/navigation'
import { Skeleton } from "@/components/ui/skeleton"
import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import { useTranslation } from 'react-i18next'
import VerifyMember from "@/app/(components)/member-verify"
import Lottie from "lottie-react";
import { userStore } from '@/app/(store)/auth-store';
import spinnerLoadingAnimation from "@/public/lotties/lotties-spinner-loading.json"
import Image from "next/image"
import { useMutation } from '@tanstack/react-query';
import { useToast } from "@/components/ui/use-toast"
import LiffErrorPage from "@/app/(components)/liff-error"

const lottiesStyle = { width: 200, height: 200 }
const SpinnerLoading = () => {
    return (
        <div className="flex flex-col w-full min-h-[80vh] h-full overflow-y-auto items-center justify-center gap-4">
            <Lottie animationData={spinnerLoadingAnimation} style={lottiesStyle} loop={true} />
        </div>
    )
}

const fetchBooking = async (id) => {
    const response = await axios.get(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/booking/${id}`);
    return response.data;
}

const paymentProof = async (booking_id) => {
    const response = await axios.put(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/payment/proofing/${booking_id}`, {}, {
        headers: {
            'Content-Type': 'application/json'
        }
    });
    return response.data;
}

const downloadImage = (imagePath) => {
    const fileName = imagePath.split('/').pop();
    
    const isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

    fetch(imagePath)
        .then(response => response.blob())
        .then(blob => {
            const blobUrl = window.URL.createObjectURL(blob);
            
            if (isIOS) {
                // สำหรับ iOS
                const reader = new FileReader();
                reader.onload = function(e) {
                    const link = document.createElement('a');
                    link.href = e.target.result;
                    link.download = fileName;
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                    window.URL.revokeObjectURL(blobUrl);
                };
                reader.readAsDataURL(blob);
            } else {
                // สำหรับ Android และ Desktop
                const link = document.createElement('a');
                link.href = blobUrl;
                link.download = fileName;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                window.URL.revokeObjectURL(blobUrl);
            }
        })
        .catch(error => console.error('Error downloading image:', error));
};

export default function Page() {

    const params = useParams()
    const router = useRouter()
    const userSession = userStore((state) => state?.userSession)
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;
    const { toast } = useToast()

    const { isPending, isError, data, error } = useQuery({
        queryKey: ['payment-processing'],
        queryFn: () => fetchBooking(params?.booking_id),
        enabled: !!params?.booking_id
    })

    const handleImageError = () => {
        setImgSrc('/services/image-not-found.png');
    };

    const handleDownload = () => {
        downloadImage('/payments/QR_KSHOP.png');
    };

    const handleProcessing = () => {
        mutationPaymentProof.mutate(data.data.id);
    }

    const handleClose = () => {
        router.back();
    }

    const mutationPaymentProof = useMutation({
        mutationFn: paymentProof,
        retry: false,
        onSuccess: (resp) => {
            if (resp?.success === true) {
                router.replace(`/service/completed/${data.data.id}`)
                return;
            } 

            toast({ description: (currentLocale === 'th' ? resp?.message : resp?.message_en) || "There was a problem with your request. please try again" })
        },
        onError: (err) => { 
            console.error("API error", err);
            toast({
                variant: "destructive",
                title: "Uh oh! Something went wrong.",
                description: "There was a problem with your request. please try again",
            })
        },
        onSettled: () => { },
        enabled: !!data?.data?.id
    })


    if (isPending) {
        return <SpinnerLoading />
    }

    if (isError) {
        console.error(error.message)
        return <LiffErrorPage />
    }

    if (data?.data && data?.data?.payment_method?.code === 'QRKSHOP') {
        return (
            <>
                <div className="flex flex-col items-center justify-center min-h-[80vh] h-full overflow-y-auto gap-4 p-10">
                    <Image className="" src={'/payments/QR_KSHOP.png'} width={350} height={350} alt="QR Scan" onError={handleImageError} />
                    <div className="text-xl font-semibold">ยอดชำระ {data.data.net_price} บาท</div>
                    <div className="w-[350px] text-sm text-gray-500">กรุณาบันทึกหน้าจอนี้เพื่อนำไปสแกนชำระที่แอปพลิเคชั่นธนาคารเท่านั้น</div>
                    <div className="w-[350px] text-sm text-gray-500">หลังจากชำระเงินเรียบร้อยแล้ว ท่านสามารถส่งสลิปหลักฐานการชำระเงินที่หน้า Chat LINE ได้เลยค่ะ</div>

                    <div className="flex flex-row justify-between w-[350px] text-sm gap-x-4">
                        <Button variant="ghost" className="w-[350px]" onClick={handleDownload}>บันทึก QR</Button>
                        {/* <Button variant="secondary" className="w-[350px]">แนบหลักฐานการชำระเงิน</Button> */}
                    </div>
                    {/* <Separator className="w-[350px]" /> */}

                    {
                        data.data.payment_status.code === 'AWAITING-PAYMENT' ?
                            <Button variant="" className="w-[350px]" onClick={handleProcessing}>ดำเนินการต่อ</Button> :
                            <Button variant="" className="w-[350px]" onClick={handleClose}>ปิด</Button>
                    }
                </div>
            </>
        )
    }

    if (data?.data && data?.data?.payment_method?.code === 'BANKTRANSFER') {
        return (
            <>
                <div className="flex flex-col items-center justify-center min-h-[80vh] h-full overflow-y-auto gap-6 p-10">
                    <div className="text-xl font-semibold">ยอดชำระ {data.data.net_price} บาท</div>
                    <div className="flex flex-row w-[350px] shadow-xl p-4 rounded-lg gap-4 border">
                        <Image className="" src={'/payments/kbank.png'} width={70} height={50} alt="Kbank icon" onError={handleImageError} />
                        <div className="flex flex-col">
                            <div>บัญชีออมทรัพย์ ธนาคาร กสิกรไทย</div>
                            <div>ชื่อบัญชี : บจก.วิบวับแวววาว</div>
                            <div>เลขบัญชี : 191-8-32131-0</div>
                        </div>
                    </div>

                    <div className="w-[350px] text-sm text-gray-500">หลังจากชำระเงินเรียบร้อยแล้ว ท่านสามารถส่งสลิปหลักฐานการชำระเงินที่หน้า Chat LINE ได้เลยค่ะ</div>

                    {
                        data.data.payment_status.code === 'AWAITING-PAYMENT' ?
                            <Button variant="" className="w-[350px]" onClick={handleProcessing}>ดำเนินการต่อ</Button> :
                            <Button variant="" className="w-[350px]" onClick={handleClose}>ปิด</Button>
                    }
                </div>
            </>
        )
    }


    return <></>
}

