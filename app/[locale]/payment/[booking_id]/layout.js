import TranslationsProvider from '@/app/providers/TranslationProvider'
import initTranslations from '@/app/i18n'

export default async function PaymentLayout({ children, params: { locale } }) {
    const i18nNamespace = ['payment'];
    const { resources } = await initTranslations(locale, i18nNamespace);
    return (
        <>
            <TranslationsProvider
                namespaces={i18nNamespace}
                locale={locale}
                resources={resources}>
                <div className="flex flex-col">
                    <div className="mb-30">{children}</div>
                </div>
            </TranslationsProvider>
        </>
    );
}