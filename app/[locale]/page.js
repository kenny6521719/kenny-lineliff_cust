
'use client'

import initTranslations from '../i18n';
import { useEffect, useState } from 'react'
import axios from 'axios';
// import ServiceCard from "../(components)/service-card"
import { useQuery } from '@tanstack/react-query'

import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from "@/components/ui/card"
import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselNext,
    CarouselPrevious,
} from "@/components/ui/carousel"
import Link from 'next/link'
import { formatNumberWithThousandSeparator } from "@/lib/numberFormat";
import { useTranslation } from 'react-i18next';
import { Skeleton } from "@/components/ui/skeleton"
import LiffErrorPage from "@/app/(components)/liff-error"
import Image from 'next/image';

function SkeletonLoading() {
    return (
      <div className="flex flex-col w-full">
        {[...Array(2)].map((_, index) => (
          <div key={index} className='flex flex-col gap-y-4 p-4 md:px-20 lg:px-40 mt-4 md:mt-10'>
            <Skeleton className="h-4 w-[200px] sm:w-[300px]" />
            <Carousel
              opts={{
                align: "start",
              }}
              className="w-full"
              orientation="horizontal"
            >
              <CarouselContent className="-ml-2 sm:-ml-4">
                {[...Array(4)].map((_, cIndex) => (
                  <CarouselItem key={cIndex} className="pl-2 sm:pl-4 basis-full sm:basis-1/2 md:basis-1/3 lg:basis-1/4">
                    <div className="p-1 w-full sm:w-[270px]">
                      <Card>
                        <Skeleton className="w-full sm:w-[260px] h-[140px]" />
                      </Card>
                      <div className="flex flex-col mt-4 gap-4">
                        <Skeleton className="h-4 w-full sm:w-[250px]" />
                        <Skeleton className="h-3 w-[80%] sm:w-[200px]" />
                      </div>
                    </div>
                  </CarouselItem>
                ))}
              </CarouselContent>
              <CarouselPrevious />
              <CarouselNext />
            </Carousel>
          </div>
        ))}
      </div>
    );
  }

export default function Home() {

    useEffect(() => {

    }, [])

    const fetchServices = async () => {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/services`);
        return response.data;
    }

    const { isPending, isError, data, error } = useQuery({
        queryKey: ['services'],
        queryFn: fetchServices,
    })

    if (isPending) {
        return <SkeletonLoading />
    }

    if (isError) {
        console.log(error.message);
        return <LiffErrorPage />
    }

    if (!data.success || !data || data?.services?.length === 0) {
        return <LiffErrorPage />
    }

    return (
        <div className='flex flex-col w-full'>
            {data.data.map((category, index) => (
                <ServiceCategory key={category.category_code} category={category} />
            ))}
        </div>
    );
}

function ServiceCategory({ category }) {
    const { i18n } = useTranslation();
    const currentLocale = i18n.language;
    return (
        <>
            <div className='flex flex-col gap-y-4 p-4 md:px-20 lg:px-40 mt-4 md:mt-10 '>
                <div className='text-xl'>{currentLocale === 'th' ? category.category_name_th : category.category_name_en}</div>
                <Carousel
                    opts={{
                        align: "start",
                    }}
                    className="w-full"
                >
                    <CarouselContent>
                        {category?.services?.map((service, index) => (
                            <ServiceCard key={service.services_code} service={service} />
                        ))}
                    </CarouselContent>
                    <CarouselPrevious />
                    <CarouselNext />
                </Carousel>
            </div>
        </>
    )
}

function ServiceCard({ service }) {
    const { i18n } = useTranslation();
    const currentLocale = i18n.language;
    const [imgSrc, setImgSrc] = useState(`/services/${service.services_code}.png`);


    const handleImageError = () => {
        setImgSrc('/services/image-not-found.png'); // ระบุ path ของรูปภาพสำรอง
    };

    return (
        <>
            {<Link href={`/service/${service.services_code}`}>
                <CarouselItem className="basis-1/2 md:basis-1/3 lg:basis-1/4">
                    <div className="p-1 w-[270px]">
                        <Card className="border-transparent border-0 shadow-none">
                            <CardContent className="p-0 flex flex-wrap aspect-video items-center justify-center overflow-hidden relative">
                                <Image
                                    // className="object-fit"
                                    src={imgSrc}
                                    alt={currentLocale === 'th' ? service.services_name_th : service.services_name_en}
                                    // width={270}
                                    // height={150}
                                    layout="fill"
                                    objectFit="cover"
                                    onError={handleImageError}
                                />
                            </CardContent>
                        </Card>
                        <div className="flex flex-col mt-4">
                            <div className="text-md font-semibold text-green-600">{currentLocale === 'th' ? service.services_name_th : service.services_name_en}</div>
                            <div className="flex flex-row w-full text-sm mt-2">
                                <span className='w-full'>
                                    { !service.price ? 'ขอใบเสนอราคา' : `เริ่มต้น ฿${formatNumberWithThousandSeparator(service.price, 0)}` }
                                    
                                </span>
                            </div>
                        </div>
                    </div>
                </CarouselItem>
            </Link>}
        </>
    )
}