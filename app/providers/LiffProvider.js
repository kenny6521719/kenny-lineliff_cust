'use client';

import React, {
  createContext,
  FC,
  PropsWithChildren,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { userStore } from '@/app/(store)/auth-store';
import axios from 'axios';
import LiffErrorPage from "@/app/(components)/liff-error"
// import { useSearchParams } from 'next/navigation';

export const fetchAndMergeUserProfiles = async (liff) => {
  try {
    const lineProfile = await liff.getProfile();
    const sysProfile = await getUserProfileFromAPI(lineProfile.userId);
    const userSession = { ...lineProfile, ...sysProfile.data };
    return userSession;
  } catch (error) {
    throw new Error(error)
  }

};

const getUserProfileFromAPI = async (uuid) => {
  const response = await axios.get(`${process.env.NEXT_PUBLIC_BASE_API}/api/liff/profile?uuid=${uuid}`);
  return response.data;
};

export const LiffContext = createContext({ liff: null, liffError: null });

export const useLiffContext = () => useContext(LiffContext);

export const LiffProvider = ({ children, liffId }) => {
  const [liff, setLiff] = useState(null);
  const [liffError, setLiffError] = useState(null);
  const setUserSession = userStore((state) => state.setUserSession);
  // const searchParams = useSearchParams();

  const initLiff = useCallback(async () => {
    try {
      const liffModule = await import('@line/liff');
      const liff = liffModule.default;
      console.log('env', process.env.NEXT_PUBLIC_APP_ENV);
      console.log('LIFF init...', liffId);

      await liff.init({ liffId, withLoginOnExternalBrowser: true });

      console.log('LIFF init succeeded.');
      setLiff(liff);

      await liff.ready;

      // const liffUrl = await liff.permanentLink.createUrlBy(window.location.href)
      // console.log("liffUrl", liffUrl);

      if (!liff.isLoggedIn()) {
        liff.login();
        return 
      }

      const userSession = await fetchAndMergeUserProfiles(liff);
      setUserSession(userSession);



    } catch (error) {
      console.log('LIFF init failed.', error);
      setLiffError((error).toString());
    }
  }, [liffId]);

  // init Liff
  useEffect(() => {
    console.log('LIFF init start...');
    initLiff();
  }, [initLiff]);

  return (
    <LiffContext.Provider value={{ liff, liffError }}>
      {liffError ? <LiffErrorPage /> : children}
    </LiffContext.Provider>
  );
};