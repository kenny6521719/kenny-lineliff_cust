import React from "react";
import { Star, StarHalf } from "lucide-react";


const StarRating = ({ rating, maxRating = 5 }) => {
    const fullStars = Math.floor(rating);
    const hasHalfStar = rating % 1 !== 0;
    return (
        <div className="flex items-center">
            {[...Array(maxRating)].map((_, index) => (
                <span key={index}>
                    {index < fullStars ? (
                        <Star className="text-yellow-400 fill-current" size={16} />
                    ) : index === fullStars && hasHalfStar ? (
                        <StarHalf className="text-yellow-400 fill-current" size={16} />
                    ) : (
                        <Star className="text-gray-300" size={16} />
                    )}
                </span>
            ))}
            <span className="ml-2 text-xs font-thin text-gray-600">{rating.toFixed(1)}</span>
        </div>
    );
};
export default StarRating;
