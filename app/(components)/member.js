'use client'

import {
    Avatar,
    AvatarFallback,
    AvatarImage,
} from "@/components/ui/avatar"

import Image from "next/image"

import { userStore } from '@/app/(store)/auth-store';
import { useTranslation } from 'react-i18next'

export default function Member() {
    const { t } = useTranslation();
    const userSession = userStore((state) => state?.userSession)

    // console.log('Member', userSession);
    return (
        <>
            <div className="flex flex-row items-center gap-x-2 cursor-pointer">
                <Avatar className="h-7 w-7">
                    <AvatarImage alt="User Avatar" src={userSession.pictureUrl} />
                </Avatar>
                <span className="text-sm">{userSession.first_name} {userSession.last_name}</span>
            </div>
        </>
    );
}