"use client"

import Lottie from "lottie-react";
import cleaningAnimatetion from "@/public/lotties/cleaning.json"
import { Button } from "@/components/ui/button"

export default function LiffErrorPage() {

    return (
        <>
            <html>
                <body>
                    <div className="h-screen w-screen flex flex-col items-center">
                        <Lottie animationData={cleaningAnimatetion} className="w-[300px] h-[300px] mt-20" />
                        <div className="mt-20 text-2xl text-blue-500">This site is under maintenance</div>
                        <div className="mt-2 text-blue-500 font-thin">We&apos;re prepairing to serve you better</div>
                        <Button className="mt-6 bg-blue-500 hover:bg-blue-500/90" onClick={() => window.location.reload()}>Reload</Button>
                    </div>
                </body>
            </html>

        </>
    );
}