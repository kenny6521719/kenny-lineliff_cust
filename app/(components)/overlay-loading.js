"use client";

import React, { useState } from "react";
import { useTranslation } from 'react-i18next'

const OverlayLoading = () => {
    return (
        <div className="fixed inset-0 z-50 flex items-center justify-center bg-black bg-opacity-50 backdrop-blur-sm">
            <div className="bg-white bg-opacity-80 rounded-lg shadow-xl p-6 max-w-sm mx-4">
                <div className="flex flex-col items-center space-y-4">
                    <div className="relative w-20 h-20">
                        <div className="absolute inset-0 rounded-full border-4 border-gray-200"></div>
                        <div className="absolute inset-0 rounded-full border-4 border-blue-500 border-t-transparent animate-spin"></div>
                        <svg
                            className="absolute inset-0 w-full h-full text-blue-500 p-4"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z"
                            />
                        </svg>
                    </div>
                    <p className="text-lg font-semibold text-gray-800">
                        กำลังโหดล...
                    </p>
                </div>
            </div>
        </div>
    );
};

export default OverlayLoading;