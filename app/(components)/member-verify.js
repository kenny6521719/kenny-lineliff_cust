"use client"

import React, { useState, useMemo, createContext, useContext, useEffect, useRef } from 'react';
import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuLabel,
    DropdownMenuSeparator,
    DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import {
    Avatar,
    AvatarFallback,
    AvatarImage,
} from "@/components/ui/avatar"
import { Button } from "@/components/ui/button"
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogFooter,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
    DialogClose
} from "@/components/ui/dialog"
import { Input } from "@/components/ui/input"
import { Label } from "@/components/ui/label"
import {
    InputOTP,
    InputOTPGroup,
    InputOTPSeparator,
    InputOTPSlot,
} from "@/components/ui/input-otp"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm, useFormState } from "react-hook-form"
import { z } from "zod"
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form"

import { useTranslation } from 'react-i18next';
import { LiffContext, fetchAndMergeUserProfiles } from '@/app/providers/LiffProvider'
import { useMutation } from '@tanstack/react-query';
import axios from 'axios';
import { userStore } from '@/app/(store)/auth-store';
import Lottie from "lottie-react";
import verifiedAnimation from '@/public/lotties/verified.json'
import { useOtpStore } from '@/app/(store)/otp-store'
import { useToast } from "@/components/ui/use-toast"
import { RotateCw } from "lucide-react"


const VerifyContext = createContext();
const StepperContext = createContext();



export default function VerifyMember({ children }) {

    const { t } = useTranslation();

    const [open, setOpen] = useState(false);
    const [isLoading, setIsLoading] = useState(false);


    const openHandler = (val) => {
        // console.log('openHandler', val);
        setOpen(val);
    };

    useEffect(() => {
        // setStepNumber(1);
        // setVerifyData({});
    }, [open])

    return (
        <>
            <Dialog open={open} onOpenChange={openHandler}>
                <DialogTrigger asChild>
                    {children}
                </DialogTrigger>
                <VerifyContent openHandler={openHandler} />
            </Dialog>
        </>
    );
}

const VerifyContent = ({ openHandler }) => {
    const [stepNumber, setStepNumber] = useState(1);
    const [verifyData, setVerifyData] = useState({});

    return (
        <DialogContent className="sm:max-w-[425px]" onInteractOutside={(e) => { e.preventDefault(); }}>
            <VerifyContext.Provider value={{ verifyData, setVerifyData }}>
                <StepperContext.Provider value={{ stepNumber, setStepNumber }}>
                    {stepNumber === 1 && <VerifyForm openHandler={openHandler} />}
                    {stepNumber === 2 && <OtpForm openHandler={openHandler} />}
                    {stepNumber === 3 && <SuccessForm openHandler={openHandler} />}
                </StepperContext.Provider>
            </VerifyContext.Provider>
        </DialogContent>
    )
}

const OtpForm = ({ openHandler }) => {
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;
    const { verifyData, setVerifyData } = useContext(VerifyContext);
    const { stepNumber, setStepNumber } = useContext(StepperContext);
    const { liff } = useContext(LiffContext);
    const { countdown, isResendEnabled, startCountdown, resetCountdown } = useOtpStore();
    const { toast } = useToast()

    const otpSchema = z.object({
        pin: z.string()
            .length(6, { message: currentLocale === "th" ? "รหัสควรมีอย่างน้อย 6 ตัว" : "OTP must have 6 digits." })
    })


    const formOTP = useForm({
        resolver: zodResolver(otpSchema),
        // mode: 'onChange',
        defaultValues: {
            pin: "",
        },
    })

    const requestVerify = async (values) => {
        const req = { ...verifyData, pin: values.pin };
        const response = await axios.post(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/verify`, req, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }

    const mutation = useMutation({
        mutationFn: requestVerify,
        retry: false,
        onSuccess: (resp) => {
            console.log("API response", resp)
            if (resp?.success === true) {
                setStepNumber(3)
                return
            }

            toast({
                description: currentLocale === 'th'? resp?.message : resp?.message_en
            })
        },
        onError: (error) => {
            console.error("API error", error);
            // Handle error, e.g., show an error message
            toast({
                variant: "destructive",
                title: "Uh oh! Something went wrong.",
                description: "There was a problem with your request. please try again",
            })
        },
        onSettled: () => {
            formOTP.setValue("pin", "")
        }
    })


    const onSubmit = async (values) => {
        mutation.mutate(values);
    };

    const onCancel = async () => {
        setStepNumber(1)
        setVerifyData({})
        openHandler(false);
    }

    const requestOTP = async (values) => {
        const response = await axios.post(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/request-otp-verify`, values, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }

    const resendMutation = useMutation({
        mutationFn: requestOTP,
        retry: false,
        onSuccess: (resp) => {
            setVerifyData((prev) => ({ ...prev, ref_code: resp.data }))
            startCountdown()
        },
        onError: (error) => {
            console.error("API error", error);
        },
        onSettled: () => {
            formOTP.setValue("pin", "")
        }
    })

    const handleResend = async (e) => {
        e.preventDefault();
        console.log('verifyData', verifyData)
        resendMutation.mutate(verifyData);
    }

    return (
        <>
            <DialogHeader>
                <DialogTitle>
                    {currentLocale === 'th' ? 'ยืนยันตัวตนด้วย OTP' : 'OTP Verification'}
                </DialogTitle>
                <DialogDescription>
                    {
                        currentLocale === 'th' ?
                            <>
                                ระบุ OTP จาก {verifyData.mobile} <br />หมายเลขอ้างอิง: {verifyData?.ref_code?.substring(verifyData?.ref_code?.length - 4)}
                            </> :
                            <>
                                Enter the OTP send to {verifyData.mobile} <br />Ref. Code: {verifyData?.ref_code?.substring(verifyData?.ref_code?.length - 4)}
                            </>
                    }

                </DialogDescription>
            </DialogHeader>
            <Form {...formOTP}>
                <form onSubmit={formOTP.handleSubmit(onSubmit)} className="flex flex-col gap-4 py-4">
                    <div className='flex flex-col items-center'>
                        <FormField
                            control={formOTP.control}
                            name="pin"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel>
                                        { currentLocale === 'th' ? <>รหัสใช้ครั้งเดียว</> : <>One-Time Password</>}
                                    </FormLabel>
                                    <FormControl>
                                        <InputOTP maxLength={6} {...field}>
                                            <InputOTPGroup>
                                                <InputOTPSlot index={0} />
                                                <InputOTPSlot index={1} />
                                                <InputOTPSlot index={2} />
                                                <InputOTPSlot index={3} />
                                                <InputOTPSlot index={4} />
                                                <InputOTPSlot index={5} />
                                            </InputOTPGroup>
                                        </InputOTP>
                                    </FormControl>
                                    {
                                        isResendEnabled ?
                                            (
                                                <div className="text-xs font-thin" onClick={handleResend}>
                                                    {
                                                        currentLocale === 'th' ?
                                                            <>
                                                                ยังไม่ได้รับ OTP ?
                                                                <span className="text-red-500 font-medium cursor-pointer underline ml-2">ส่งอีกครั้ง</span>
                                                            </> :
                                                            <>
                                                                Don&apos;t receive the OTP ?
                                                                <span className="text-red-500 font-medium cursor-pointer underline ml-2">RESEND OTP</span>
                                                            </>
                                                    }

                                                </div>
                                            ) :
                                            (
                                                <div className="text-xs font-thin">
                                                    {
                                                        currentLocale === 'th' ?
                                                            <>
                                                                ยังไม่ได้รับ OTP ?
                                                                <span className="text-slate-400 font-medium cursor-not-allowed underline ml-2">ส่งอีกครั้ง {`(${countdown})`}</span>
                                                            </> :
                                                            <>
                                                                Don&apos;t receive the OTP ?
                                                                <span className="text-slate-400 font-medium cursor-not-allowed underline ml-2">RESEND OTP {`(${countdown})`}</span>
                                                            </>
                                                    }

                                                </div>
                                            )
                                    }
                                    <FormMessage />
                                </FormItem>
                            )}
                        />
                    </div>

                    <DialogFooter>
                        <div className='flex flex-col-reverse sm:flex-row gap-4 mt-4'>
                            <Button type="button" variant="secondary" disabled={mutation.isPending} onClick={onCancel}>
                                {currentLocale === 'th' ? 'ยกเลิก' : 'Cancel'}
                            </Button>
                            <Button type="submit" disabled={mutation.isPending || resendMutation.isPending}>
                                {
                                    mutation.isPending ?
                                        <RotateCw className="mr-2 h-4 w-4 animate-spin" /> :
                                        currentLocale === 'th' ? 'ยืนยัน' : 'Verify'
                                }
                            </Button>
                        </div>
                    </DialogFooter>
                </form>
            </Form>
        </>
    );
}

const VerifyForm = ({ openHandler }) => {
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;

    const { verifyData, setVerifyData } = useContext(VerifyContext);
    const { stepNumber, setStepNumber } = useContext(StepperContext);
    const { liff } = useContext(LiffContext);
    const { countdown, isResendEnabled, startCountdown, resetCountdown } = useOtpStore();
    const { toast } = useToast()

    const formSchema = z.object({
        first_name: z.string().min(2, {
            message: currentLocale === "th" ? "ชื่อควรมีอย่างน้อย 2 ตัวอักษร" : "First name must be at least 2 characters.",
        }),
        last_name: z.string().min(2, {
            message: currentLocale === "th" ? "นามสกุลควรมีอย่างน้อย 2 ตัวอักษร" : "Last name must be at least 2 characters.",
        }),
        mobile: z.string()
            .min(10, {
                message: currentLocale === "th" ? "เบอร์มือถือต้องมี 10 หลัก" : "Mobile number must have 10 digits.",
            })
            .max(10, {
                message: currentLocale === "th" ? "เบอร์มือถือต้องมี 10 หลัก" : "Mobile number must have 10 digits.",
            }),
        email: z.union([
            z.literal(''),
            z.string().email({ message: currentLocale === "th" ? "รูปแบบอีเมลไม่ถูกต้อง" : "Email address is invalid format."}),
        ])

    })

    const form = useForm({
        resolver: zodResolver(formSchema),
        mode: 'onChange',
        defaultValues: {
            first_name: "",
            last_name: "",
            mobile: "",
            email: "",
        },
    })

    const requestOTP = async (values) => {
        const lineProfile = await liff.getProfile();
        // console.log('onSubmit 2 ', lineProfile);
        let req = {
            line_uuid: lineProfile.userId,
            mobile: values.mobile,
            email: values.email,
            first_name: values.first_name,
            last_name: values.last_name,
        };
        setVerifyData(req);

        const response = await axios.post(`${process.env.NEXT_PUBLIC_BASE_API}/api/Liff/request-otp-verify`, req, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return response.data;
    }

    const mutation = useMutation({
        mutationFn: requestOTP,
        retry: false,
        onSuccess: (resp) => {
            console.log("API response", resp);
            if (!resp.success) {
                openHandler(false)
                toast({
                    description: currentLocale === 'th' ? resp?.message : resp?.message_en,
                })
                return;
            }
            // Set next step
            setVerifyData((prev) => ({ ...prev, ref_code: resp.data }))
            startCountdown()
            setStepNumber(2)
        },
        onError: (error) => {
            console.error("API error", error);
            // Handle error, e.g., show an error message
        }
    })

    const onSubmit = async (values) => {
        // console.log('onSubmit', values);

        //execute api
        mutation.mutate(values);
    };

    return (
        <>
            <DialogHeader>
                <DialogTitle>
                    {currentLocale === 'th' ? 'ยืนยันตัวตน' : 'Verify'}
                </DialogTitle>
                <DialogDescription>
                    {
                        currentLocale === 'th' ?
                            'โปรดระบุข้อมูลเพื่อยืนยันตัวตนในการเข้ารับบริการ' :
                            'Please provide information to verify your identity for receiving the service'
                    }
                </DialogDescription>
            </DialogHeader>

            <Form {...form}>
                <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col gap-4 py-4">
                    <FormField
                        control={form.control}
                        name="first_name"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>
                                    {currentLocale === 'th' ? 'ชื่อ' : 'First Name'}
                                    <span className="text-red-500"> *</span>
                                </FormLabel>
                                <FormControl>
                                    <Input placeholder="" {...field} />
                                </FormControl>
                                <FormMessage className="text-xs" />
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="last_name"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>
                                    {currentLocale === 'th' ? 'นามสกุล' : 'Last Name'}
                                    <span className="text-red-500"> *</span>
                                </FormLabel>
                                <FormControl>
                                    <Input placeholder="" {...field} />
                                </FormControl>
                                <FormMessage className="text-xs" />
                            </FormItem>
                        )}
                    />
                    <FormField
                        control={form.control}
                        name="mobile"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>
                                    {currentLocale === 'th' ? 'เบอร์มือถือ' : 'Mobile No.'}
                                    <span className="text-red-500"> *</span>
                                </FormLabel>
                                <FormControl>
                                    <Input placeholder="" {...field} />
                                </FormControl>
                                <FormMessage className="text-xs" />
                            </FormItem>
                        )}
                    />
                    {/* <FormField
                        control={form.control}
                        name="email"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>
                                    {currentLocale === 'th' ? 'อีเมล' : 'Email'}
                                </FormLabel>
                                <FormControl>
                                    <Input placeholder="" {...field} />
                                </FormControl>
                                <FormMessage className="text-xs" />
                            </FormItem>
                        )}
                    /> */}
                    <DialogFooter>
                        <Button className="mt-4" type="submit" disabled={mutation.isPending || !isResendEnabled}>
                            {
                                mutation.isPending ?
                                    <RotateCw className="mr-2 h-4 w-4 animate-spin" /> :
                                    currentLocale === 'th' ? 'ยืนยัน' : "Proceed"
                            }
                            {!isResendEnabled && `(${countdown})`}
                        </Button>
                    </DialogFooter>
                </form>
            </Form>
        </>
    );
}

const SuccessForm = ({ openHandler }) => {
    const { t, i18n } = useTranslation();
    const currentLocale = i18n.language;
    const { verifyData, setVerifyData } = useContext(VerifyContext);
    const { stepNumber, setStepNumber } = useContext(StepperContext);
    const [count, setCount] = useState(5);
    const closeButtonRef = useRef(null);
    const setUserSession = userStore((state) => state.setUserSession);

    const fetchProfile = async () => {
        if (liff) {
            try {
                const profile = await fetchAndMergeUserProfiles(liff);
                setUserSession(profile);
            } catch (error) {
                console.error('Failed to fetch user profile:', error);
            }
        }
    };

    const handleClose = () => {
        openHandler(false);
        fetchProfile();
        setStepNumber(1);
        setVerifyData({});
        closeButtonRef.current.click();
    };

    useEffect(() => {
        if (count === 0) {
            handleClose();
            return;
        }

        const timerId = setInterval(() => {
            setCount(prevCount => prevCount - 1);
        }, 1000);

        return () => clearInterval(timerId);
    }, [count]);

    return (
        <>
            <div className='flex flex-col items-center'>
                <Lottie animationData={verifiedAnimation} />
                <div className='text-sm text-center leading-loose'>
                    {
                        currentLocale === 'th' ?
                            <>คุณ <span className='text-green-500'>{verifyData.first_name} {verifyData.last_name} <br /></span> ได้เป็นสมาชิกใหม่ของเราเรียบร้อยแล้ว เชิญเลือกใช้บริการของเราได้เลยค่ะ</> :
                            <>You have successfully add <br /><span className='text-green-500'>{verifyData.first_name} {verifyData.last_name}</span> as a new member</>
                    }

                </div>
                <DialogFooter>
                    <div className='flex sm:flex-row gap-4 mt-6'>
                        <Button type="button" className="bg-green-500" ref={closeButtonRef}>
                            {currentLocale === 'th' ? <>สำเร็จ ({count})</> : <>Completed ({count})</>}
                        </Button>
                    </div>
                </DialogFooter>
            </div>
        </>
    )
}

const UserIcon = (props) => {
    return (
        <svg
            {...props}
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="M19 21v-2a4 4 0 0 0-4-4H9a4 4 0 0 0-4 4v2" />
            <circle cx="12" cy="7" r="4" />
        </svg>
    )
}
