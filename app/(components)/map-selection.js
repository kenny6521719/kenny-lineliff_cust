"use client"

import React, { useState, useEffect, useRef, useCallback } from "react"
import { CalendarDays, ChevronRight, MapPin, Plus, LocateFixed, MoveLeft, ArrowLeft, PencilLine, Search, CircleX } from "lucide-react";
import {
    Sheet,
    SheetContent,
    SheetDescription,
    SheetFooter,
    SheetHeader,
    SheetTitle,
    SheetTrigger,
} from "@/components/ui/sheet"
import {
    Command,
    CommandEmpty,
    CommandGroup,
    CommandInput,
    CommandItem,
    CommandList,
} from "@/components/ui/command"
import {
    Popover,
    PopoverContent,
    PopoverTrigger,
} from "@/components/ui/popover"
import { Button } from "@/components/ui/button"
import { GoogleMap, useJsApiLoader, Marker } from "@react-google-maps/api";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form";
import { useTranslation } from 'react-i18next'
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea"
import spinnerLoadingAnimation from "@/public/lotties/lotties-spinner-loading.json"
import Lottie from "lottie-react";
import { addressHandlerStore } from '@/app/(store)/address-store'
import { Separator } from "@/components/ui/separator"

const lottiesStyle = { width: 200, height: 200 }

const SpinnerLoading = () => {
    return (
        <div className="flex flex-col w-full min-h-[80vh] h-full overflow-y-auto items-center justify-center gap-4">
            <Lottie animationData={spinnerLoadingAnimation} style={lottiesStyle} loop={true} />
        </div>
    )
}

export default function MapSelectionPage({ address }) {
    const addressStore = addressHandlerStore((state) => state.address);
    const setAddressStore = addressHandlerStore((state) => state.setAddress);

    const [coordinateX, setCoordinateX] = useState("")
    const [coordinateY, setCoordinateY] = useState("")
    const [fullAddress, setFullAddress] = useState("")

    const [isPlacesServiceReady, setIsPlacesServiceReady] = useState(false);
    const placesServiceRef = useRef(null);
    const searchAutocompleteRef = useRef(null);
    const [searchResults, setSearchResults] = useState([]);
    const [searchAddressValue, setSearchAddressValue] = useState("")
    const [isSearchFocused, setIsSearchFocused] = useState(false);
    const handleSearchFocus = () => {
        console.log("focus")
        return setIsSearchFocused(true);
    };
    const handleSearchBlur = () => {
        console.log("un-focus")
        return setIsSearchFocused(false);
    };

    const [isLoading, setIsLoading] = useState(false);
    const [openDialog, setOpenDialog] = useState(false)
    const [openSearch, setOpenSearch] = useState(false)
    const [mapCenter, setMapCenter] = useState(() => {
        if (address?.address_coordinate_x && address?.address_coordinate_y) {
            return {
                lat: parseFloat(address.address_coordinate_x),
                lng: parseFloat(address.address_coordinate_y)
            };
        }
        return { lat: 13.7563, lng: 100.5018 }; // Bangkok coordinates as default
    });
    const [libraries] = useState(['marker', 'geocoding', 'places']);
    const [marker, setMarker] = useState();
    const mapRef = useRef(null);
    const markerRef = useRef(null);
    const geocoder = useRef(null);

    const { isLoaded, loadError } = useJsApiLoader({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY,
        libraries,
    });

    const initAutocomplete = useCallback(() => {
        if (!isLoaded) return;
        searchAutocompleteRef.current = new window.google.maps.places.AutocompleteService();
    }, [isLoaded]);


    const updateFormCoordinates = useCallback((lat, lng) => {
        setCoordinateX(lat.toString())
        setCoordinateY(lng.toString())
    }, []);

    const reverseGeocode = useCallback((lat, lng, updateStore = false) => {
        if (geocoder.current) {
            geocoder.current.geocode({ location: { lat, lng } }, (results, status) => {
                console.log("reverseGeocode", results);
                if (status === "OK" && results[0]) {
                    setFullAddress(results[0].formatted_address);
                    if (updateStore) {
                        setAddressStore({
                            full_address: results[0].formatted_address,
                            address_coordinate_x: lat.toString(),
                            address_coordinate_y: lng.toString()
                        });
                    }
                } else {
                    console.log("ไม่สามารถค้นหาที่อยู่ได้: " + status);
                }
            });
        }
    }, [setAddressStore]);

    const updateMarkerPosition = useCallback((newPos) => {
        console.log("updateMarkerPosition", newPos);
        if (marker) {
            if (marker instanceof window.google.maps.marker.AdvancedMarkerElement) {
                marker.position = new window.google.maps.LatLng(newPos.lat, newPos.lng);
            } else {
                marker.setPosition(newPos);
            }
        }
        setMapCenter(newPos);
        updateFormCoordinates(newPos.lat, newPos.lng);
        // Only update address if it's a user action, not initial load
        reverseGeocode(newPos.lat, newPos.lng, true);
    }, [marker, updateFormCoordinates, reverseGeocode]);

    const getCurrentLocation = useCallback((forceUpdate = false) => {
        if (!forceUpdate && address?.address_coordinate_x && address?.address_coordinate_y) {
            console.log("Using provided coordinates");
            const newPos = {
                lat: parseFloat(address.address_coordinate_x),
                lng: parseFloat(address.address_coordinate_y)
            };
            setMapCenter(newPos);
            updateMarkerPosition(newPos);
            setFullAddress(address.full_address || "");
            return;
        }

        setIsLoading(true);
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    const newPos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    setMapCenter(newPos);
                    updateMarkerPosition(newPos);
                    // Don't update store here, just get the address
                    reverseGeocode(newPos.lat, newPos.lng, false);
                    setIsLoading(false);
                },
                (error) => {
                    console.error("Error getting location:", error);
                    const defaultPos = { lat: 13.7563, lng: 100.5018 };
                    setMapCenter(defaultPos);
                    updateMarkerPosition(defaultPos);
                    setIsLoading(false);
                },
                { enableHighAccuracy: true, timeout: 10000, maximumAge: 0 }
            );
        } else {
            console.error("Geolocation is not supported by this browser.");
            const defaultPos = { lat: 13.7563, lng: 100.5018 };
            setMapCenter(defaultPos);
            updateMarkerPosition(defaultPos);
            setIsLoading(false);
        }
    }, [address, updateMarkerPosition, reverseGeocode]);

    const handleMarkerDragEnd = useCallback((e) => {
        const newPos = e.latLng ? { lat: e.latLng.lat(), lng: e.latLng.lng() } : marker.position;
        setMapCenter(newPos);
        updateFormCoordinates(newPos.lat, newPos.lng);
        reverseGeocode(newPos.lat, newPos.lng, true);
    }, [marker, updateFormCoordinates, reverseGeocode]);

    const handleMapClick = useCallback((e) => {
        const newPos = { lat: e.latLng.lat(), lng: e.latLng.lng() };
        setMapCenter(newPos);
        if (markerRef.current) {
            if (markerRef.current instanceof window.google.maps.marker.AdvancedMarkerElement) {
                markerRef.current.position = new window.google.maps.LatLng(newPos.lat, newPos.lng);
            } else {
                markerRef.current.setPosition(newPos);
            }
        }
        updateFormCoordinates(newPos.lat, newPos.lng);
        reverseGeocode(newPos.lat, newPos.lng, true);
    }, [updateFormCoordinates, reverseGeocode]);

    const onMapLoad = useCallback(
        (map) => {
            mapRef.current = map;
            placesServiceRef.current = new window.google.maps.places.PlacesService(map);
            setIsPlacesServiceReady(true);

            const { AdvancedMarkerElement } = window.google.maps.marker;

            if (AdvancedMarkerElement) {
                const newMarker = new AdvancedMarkerElement({
                    map,
                    position: new window.google.maps.LatLng(mapCenter.lat, mapCenter.lng),
                    gmpDraggable: true,
                });
                newMarker.addListener("dragend", handleMarkerDragEnd);
                markerRef.current = newMarker;
            } else {
                console.warn(
                    "AdvancedMarkerElement is not available. Falling back to standard Marker."
                );
                const newMarker = new window.google.maps.Marker({
                    map,
                    position: mapCenter,
                    draggable: true,
                });
                newMarker.addListener("dragend", handleMarkerDragEnd);
                markerRef.current = newMarker;
            }

            map.setCenter(mapCenter);
            map.setZoom(17);

            updateFormCoordinates(mapCenter.lat, mapCenter.lng);

            map.addListener("click", handleMapClick);
        },
        [mapCenter, handleMarkerDragEnd, handleMapClick, updateFormCoordinates]
    );

    const onCurrentLocation = (e) => {
        e.preventDefault()
        getCurrentLocation(true)
    }

    useEffect(() => {


        return () => {
            // if (markerRef.current) {
            //     markerRef.current.setMap(null);
            // }

        };
    }, []);

    useEffect(() => {
        if (isLoaded) {
            initAutocomplete();
        }
    }, [isLoaded, initAutocomplete]);

    useEffect(() => {
        if (mapRef.current && markerRef.current) {
            mapRef.current.panTo(mapCenter);
            if (markerRef.current instanceof window.google.maps.marker.AdvancedMarkerElement) {
                markerRef.current.position = new window.google.maps.LatLng(mapCenter.lat, mapCenter.lng);
            } else {
                markerRef.current.setPosition(mapCenter);
            }
            updateFormCoordinates(mapCenter.lat, mapCenter.lng);
        }
    }, [mapCenter, updateFormCoordinates]);

    // useEffect(() => {
    //     if (isLoaded && !geocoder.current) {
    //         geocoder.current = new window.google.maps.Geocoder();
    //     }
    // }, [isLoaded])

    const initServices = useCallback(() => {
        if (!isLoaded) return;
        searchAutocompleteRef.current = new window.google.maps.places.AutocompleteService();
        geocoder.current = new window.google.maps.Geocoder();
        setIsPlacesServiceReady(true);
    }, [isLoaded]);

    useEffect(() => {
        if (isLoaded) {
            initServices();
        }
    }, [isLoaded, initServices]);

    useEffect(() => {
        if (mapRef.current && markerRef.current) {
            mapRef.current.panTo(mapCenter);
            if (markerRef.current instanceof window.google.maps.marker.AdvancedMarkerElement) {
                markerRef.current.position = new window.google.maps.LatLng(mapCenter.lat, mapCenter.lng);
            } else {
                markerRef.current.setPosition(mapCenter);
            }
        }
    }, [mapCenter]);

    useEffect(() => {
        if (!isLoaded) return;

        console.log("useEffect address", address);

        if (address && address.address_coordinate_x && address.address_coordinate_y) {
            const newCenter = {
                lat: parseFloat(address.address_coordinate_x),
                lng: parseFloat(address.address_coordinate_y)
            };
            setMapCenter(newCenter);
            setFullAddress(address.full_address || "");
        } else if (address && address.full_address) {
            setFullAddress(address.full_address);
            // If we only have an address, we might want to geocode it to get coordinates
            // For now, we'll use default coordinates
            setMapCenter({ lat: 13.7563, lng: 100.5018 });
        } else {
            getCurrentLocation(false);
        }

        setAddressStore({
            address_coordinate_x: address?.address_coordinate_x || null,
            address_coordinate_y: address?.address_coordinate_y || null,
            full_address: address?.full_address || "",
            address_detail: address?.address_detail || "",
        });

    }, [address, getCurrentLocation, setAddressStore, isLoaded]);

    useEffect(() => {
        if (isLoaded && mapRef.current) {
            placesServiceRef.current = new window.google.maps.places.PlacesService(mapRef.current);
        }
    }, [isLoaded]);

    // const handleSearchChange = useCallback((e) => {
    //     const value = e.target.value;
    //     setSearchAddressValue(value);
    //     if (value.length > 2 && searchAutocompleteRef.current) {
    //         searchAutocompleteRef.current.getPlacePredictions(
    //             { input: value },
    //             (predictions, status) => {
    //                 if (status === window.google.maps.places.PlacesServiceStatus.OK && predictions) {
    //                     setSearchResults(predictions);
    //                 }
    //             }
    //         );
    //     } else {
    //         setSearchResults([]);
    //     }
    // }, []);
    const handleSearchChange = useCallback((e) => {
        const value = e.target.value;
        setSearchAddressValue(value);

        // ตรวจสอบว่า input เป็นรูปแบบของ lat,long หรือไม่
        const latLongRegex = /^(-?\d+(\.\d+)?),\s*(-?\d+(\.\d+)?)$/;
        const isLatLong = latLongRegex.test(value);

        if (isLatLong) {
            const [lat, lng] = value.split(',').map(coord => parseFloat(coord.trim()));
            if (!isNaN(lat) && !isNaN(lng)) {
                // ตรวจสอบว่าพิกัดอยู่ในขอบเขตของประเทศไทยหรือไม่
                if (lat >= 5.61 && lat <= 20.47 && lng >= 97.34 && lng <= 105.64) {
                    reverseGeocode(lat, lng, false);
                    setSearchResults([{
                        description: `Latitude: ${lat}, Longitude: ${lng}`,
                        place_id: 'custom_latlong',
                        lat,
                        lng
                    }]);
                } else {
                    setSearchResults([{ description: "พิกัดอยู่นอกประเทศไทย", place_id: 'invalid_latlong' }]);
                }
            }
        } else if (value.length > 2 && searchAutocompleteRef.current) {
            // ค้นหาเฉพาะในประเทศไทย
            searchAutocompleteRef.current.getPlacePredictions(
                {
                    input: value,
                    componentRestrictions: { country: 'th' }
                },
                (predictions, status) => {
                    if (status === window.google.maps.places.PlacesServiceStatus.OK && predictions) {
                        setSearchResults(predictions);
                    } else {
                        setSearchResults([]);
                    }
                }
            );
        } else {
            setSearchResults([]);
        }
    }, [reverseGeocode]);

    const handleSelectPlace = useCallback((place) => {
        console.log("Selected place:", place);
        setSearchAddressValue(place.description);
        setSearchResults([]);
        setIsSearchFocused(false);

        if (place.place_id === 'custom_latlong') {
            // กรณีที่เลือกจากการค้นหาด้วยพิกัด
            const newPos = { lat: place.lat, lng: place.lng };
            setMapCenter(newPos);
            updateMarkerPosition(newPos);
            reverseGeocode(newPos.lat, newPos.lng, true);
        } else if (placesServiceRef.current && isPlacesServiceReady) {
            // กรณีที่เลือกจากผลการค้นหาปกติ
            placesServiceRef.current.getDetails(
                { placeId: place.place_id, fields: ['geometry'] },
                (result, status) => {
                    if (status === window.google.maps.places.PlacesServiceStatus.OK) {
                        console.log("Place details:", result);
                        const newPos = {
                            lat: result.geometry.location.lat(),
                            lng: result.geometry.location.lng()
                        };
                        setMapCenter(newPos);
                        updateMarkerPosition(newPos);
                        reverseGeocode(newPos.lat, newPos.lng, true);
                    } else {
                        console.error("Error fetching place details:", status);
                        // alert("ไม่สามารถดึงข้อมูลสถานที่ได้ กรุณาลองใหม่อีกครั้ง");
                    }
                }
            );
        } else {
            console.error("Places service is not initialized");
            // alert("ระบบยังไม่พร้อมใช้งาน กรุณารอสักครู่แล้วลองใหม่อีกครั้ง");
        }
    }, [isPlacesServiceReady, setMapCenter, updateMarkerPosition, reverseGeocode]);


    // const handleSelectPlace = useCallback((place) => {
    //     console.log("Selected place:", place);
    //     setSearchAddressValue(place.description);
    //     setSearchResults([]);
    //     setIsSearchFocused(false);

    //     if (placesServiceRef.current) {
    //         placesServiceRef.current.getDetails(
    //             { placeId: place.place_id },
    //             (result, status) => {
    //                 if (status === window.google.maps.places.PlacesServiceStatus.OK) {
    //                     console.log("Place details:", result);
    //                     const newPos = {
    //                         lat: result.geometry.location.lat(),
    //                         lng: result.geometry.location.lng()
    //                     };
    //                     setMapCenter(newPos);
    //                     updateMarkerPosition(newPos);
    //                     reverseGeocode(newPos.lat, newPos.lng, true);
    //                 } else {
    //                     console.error("Error fetching place details:", status);
    //                 }
    //             }
    //         );
    //     } else {
    //         console.error("Places service is not initialized");
    //     }
    // }, [setMapCenter, updateMarkerPosition, reverseGeocode]);

    const handleClose = () => {
        setOpenDialog(false);
    }

    const onSubmit = () => {

        const result = {
            full_address: addressStore.full_address,
            address_coordinate_x: addressStore.address_coordinate_x || coordinateX,
            address_coordinate_y: addressStore.address_coordinate_y || coordinateY,
        }

        setAddressStore({ ...result })
        setOpenDialog(false)
    }

    const searchOnFoucs = () => {
        console.log("search focus")
    }

    return (<>
        <Sheet open={openDialog} onOpenChange={setOpenDialog}>
            <SheetTrigger>
                <div className="p-2 flex flex-row bg-green-200 rounded-sm h-[40px] mt-2 justify-between items-center cursor-pointer w-full">

                    {
                        (fullAddress) ?
                            <>
                                <div className="flex flex-row items-center gap-2 w-full text-sm p-2">
                                    {<span className="truncate">{fullAddress}</span>}
                                    <ChevronRight size={36} strokeWidth={3} />
                                </div>
                            </> :
                            <>
                                <div className="flex flex-row items-center justify-between gap-2 w-full">
                                    <div className="flex flex-row items-center gap-2">
                                        <MapPin size={16} strokeWidth={2} />
                                        <span className="text-sm">Choose from the map</span>
                                    </div>
                                    <ChevronRight size={16} strokeWidth={3} />
                                </div>
                            </>
                    }

                </div>
            </SheetTrigger>
            <SheetContent onOpenAutoFocus={(e) => e.preventDefault()} showCloseIcon={false} side="bottom" className="w-screen max-w-screen h-screen p-0">
                <SheetHeader className="px-4 pt-2 pb-2">
                    <SheetTitle>
                        <div className="flex flex-row items-center mt-8 gap-4">
                            <ArrowLeft size={24} strokeWidth={3} onClick={handleClose} />

                            <div className="flex flex-row items-center w-full relative">
                                <Search className="absolute left-3 top-1/2 transform -translate-y-1/2 text-gray-400" strokeWidth={1} />
                                <Input className=" focus-visible:ring-transparent placeholder:font-normal placeholder:text-gray-400 font-light pl-10 pr-10"
                                    type="text"
                                    placeholder="Search Address"
                                    autoComplete="nope"
                                    autoFocus={false}
                                    value={searchAddressValue}
                                    onChange={handleSearchChange}
                                    onFocus={handleSearchFocus}
                                    onBlur={handleSearchBlur} />
                                {searchAddressValue && <CircleX className="absolute right-3 top-1/2 transform -translate-y-1/2 text-gray-500 cursor-pointer" strokeWidth={2} onClick={() => setSearchAddressValue("")} />}
                            </div>

                        </div>
                    </SheetTitle>
                    <SheetDescription></SheetDescription>
                </SheetHeader>
                {isSearchFocused ?
                    <>
                        {searchResults.map((result, index) => (
                            <React.Fragment key={index}>
                                <div
                                    className="px-4 hover:bg-gray-100 cursor-pointer text-sm"
                                    onMouseDown={() => handleSelectPlace(result)}
                                >
                                    {result.description}
                                </div>
                                <Separator className="my-4" />
                            </React.Fragment>
                        ))}
                    </> :
                    <div className="h-full w-full flex flex-col justify-start">
                        <div className="relative h-full bg-gray-100">
                            {isLoaded && <GoogleMap
                                mapContainerStyle={{ width: "100%", height: "70%" }}
                                center={mapCenter}
                                zoom={17}
                                onClick={handleMapClick}
                                onLoad={onMapLoad}
                                options={{
                                    mapId: process.env.NEXT_PUBLIC_GOOGLE_MAPS_ID,
                                    disableDefaultUI: true,
                                    gestureHandling: 'greedy',
                                    mapTypeControl: false,
                                    fullscreenControl: false,
                                    streetViewControl: false,
                                    clickableIcons: false,
                                }}
                            >
                            </GoogleMap>}
                            {isLoading && (
                                <div className="absolute inset-0 bg-black bg-opacity-50 flex items-center justify-center z-10">
                                    <SpinnerLoading />
                                </div>
                            )}

                            
                        </div>
                        <div className="p-6 flex flex-col gap-2 fixed bottom-0 w-full h-[30vh] rounded-t-lg bg-white justify-between">
                            <div className="flex flex-col gap-2">
                                <div className="text-xl">Service address</div>
                                <div className="flex flex-row gap-6">
                                    <div className="font-light text-gray-600">{fullAddress}</div>
                                    <div className="flex-1">
                                        <EditAddress />
                                    </div>
                                </div>
                            </div>

                            <Button className=" h-[50px] bg-green-600 font-medium text-lg" onClick={onSubmit} disabled={isLoading} >Confirm this address</Button>

                            <div onClick={onCurrentLocation} className="h-12 w-12 rounded-full bg-white absolute -top-14 right-6 cursor-pointer flex justify-center items-center drop-shadow-xl border">
                                <LocateFixed size={24} strokeWidth={2} />
                            </div>

                        </div>
                    </div>}
            </SheetContent>
        </Sheet>
    </>);
}

const EditAddress = () => {
    const { t, i18n } = useTranslation();
    const [open, setOpen] = useState(false)

    const addressStore = addressHandlerStore((state) => state.address);
    const setAddressStore = addressHandlerStore((state) => state.setAddress);


    const formSchema = z.object({
        full_address: z.string().min(1, { message: "โปรดระบุที่อยู่ในการเข้ารับบริการ" }),
        address_detail: z.string().nullable(),
    });

    // const initialValues = {
    //     full_address: addressStore?.full_address || "",
    //     detail: addressStore?.address_detail || "",
    // };

    const form = useForm({
        resolver: zodResolver(formSchema),
        // defaultValues: initialValues,
    });

    const handleClose = () => {
        setOpen(false);
    }

    const onSubmit = () => {
        setAddressStore({ ...form.getValues() })
        setOpen(false)
    }

    useEffect(() => {
        form.setValue("full_address", addressStore.full_address)
        form.setValue("address_detail", addressStore.address_detail)
        return () => { }
    }, [addressStore])

    return (<>
        <Sheet open={open} onOpenChange={setOpen}>
            <SheetTrigger asChild>
                <Button className="bg-transparent hover:bg-slate-50">
                    <PencilLine size={26} strokeWidth={2} className="text-green-600" />
                </Button>
            </SheetTrigger>
            <SheetContent showCloseIcon={false} className="w-screen max-w-screen h-full p-0">
                <SheetHeader className="px-4 pt-2 pb-2">
                    <SheetTitle>
                        <div className="flex flex-row items-center mt-6 gap-4">
                            <ArrowLeft size={24} strokeWidth={3} onClick={handleClose} />
                            <div>Edit address</div>
                        </div>
                    </SheetTitle>
                    <SheetDescription></SheetDescription>
                </SheetHeader>
                <div className="h-full w-full flex flex-col p-6 justify-between">
                    <div className="flex flex-col">
                        <Form {...form}>
                            <form className="h-full">
                                <div className="flex flex-col mt-4  gap-2">
                                    <FormField
                                        control={form.control}
                                        name="full_address"
                                        render={({ field }) => (
                                            <FormItem>
                                                <FormLabel>
                                                    {t("dialog_address_info.map_address_detail")}<span className="text-red-500"> *</span>
                                                </FormLabel>
                                                <FormControl>
                                                    <Textarea placeholder="" {...field} className="resize-none" rows="2" />
                                                </FormControl>
                                                <FormDescription> </FormDescription>
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />

                                    <FormField
                                        control={form.control}
                                        name="address_detail"
                                        render={({ field }) => (
                                            <FormItem>
                                                <FormLabel>
                                                    {t("dialog_address_info.map_address_landmark")}
                                                </FormLabel>
                                                <FormControl>
                                                    <Input placeholder="" {...field} className="resize-none" rows="2" />
                                                </FormControl>
                                                <FormDescription> </FormDescription>
                                                <FormMessage />
                                            </FormItem>
                                        )}
                                    />
                                </div>
                            </form>

                        </Form>
                    </div>
                    <div className="flex flex-col justify-end">
                        <Button className="mb-20 h-[50px] bg-green-600 font-medium text-lg" onClick={onSubmit}>Save</Button>
                    </div>

                </div>
            </SheetContent>
        </Sheet>
    </>);
}