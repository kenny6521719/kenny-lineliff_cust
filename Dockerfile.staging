
# Stage 1
FROM node:20-alpine as dependencies

WORKDIR /app

COPY package.json package-lock.json next.config.mjs ./
COPY .env/.env.staging .env/.env.staging

RUN npm i

#Stage 2 - Build
FROM node:20-alpine as builder

WORKDIR /app

COPY --from=dependencies /app/node_modules ./node_modules
COPY --from=dependencies /app/.env/.env.staging .env/.env.staging
COPY . .

RUN npm run build:stg

# Debugging step to list the contents of the standalone directory
RUN ls -l /app/.next/standalone

# Stage 3 Runner
FROM node:20-alpine as runner

WORKDIR /app

COPY --from=builder /app/.next/standalone /app/standalone
COPY --from=builder /app/public /app/standalone/public
COPY --from=builder /app/.next/static /app/standalone/.next/static
COPY --from=builder /app/.env/.env.staging /app/standalone/.env/.env.staging

EXPOSE 3000

CMD HOSTNAME="0.0.0.0" node ./standalone/server.js